<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 1</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	session_start();

	if (isset($_POST['env']) && !empty($_POST['user']) && !empty($_POST['password'])) {
		$_SESSION['user']=$_POST['user'];
		$_SESSION['password']=$_POST['password'];

	?>
	
	<p>Los datos introducidos son los siguientes: </p>
	<p>Usuario: <?php echo $_SESSION['user']; ?></p>
	<p>Contraseña: <?php echo $_SESSION['password']; ?></p>

	<?php	
	}
	else {
		echo "<h2>Error: Algún dato sin rellenar</h2><p>Se le redigirá a la página anterior</p>";
		header("Refresh:3;URL=E1_index.php");
	}

	?>
</body>
</html>