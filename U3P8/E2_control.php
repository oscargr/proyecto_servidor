<?php

session_start();

if (isset($_POST['add'])) {
	$prod = $_POST['add'];
	$_SESSION['carro'][$prod]++;
	header("Location:E2_productos.php");
}

if (isset($_POST['del'])) {
	if ($_SESSION['carro'][$_POST['del']] > 0) {
		$prod = $_POST['del'];
		$_SESSION['carro'][$prod]--;
		header("Location:E2_productos.php");
	}
}

if (isset($_POST['fc'])) {
	header("Location:E2_confirmar.php");
}

?>