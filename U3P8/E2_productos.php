<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Dancing+Script|Lobster|Permanent+Marker&display=swap');
	</style>
</head>
<body>
	<?php

	session_start();

		$libros = array(
			'Cuentos completos'=>array('Imagen'=>"https://imagessl3.casadellibro.com/a/l/t0/13/9788426420213.jpg",'Título'=>"Cuentos completos",'Autor'=>"Jorge Luis Borges",'Editorial'=>"Lumen",'Idioma'=>"Castellano",'Precio'=>32.90),
			'H.P. Lovecraft anotado'=>array('Imagen'=>"https://imagessl7.casadellibro.com/a/l/t0/67/9788446043867.jpg",'Título'=>"H.P. Lovecraft anotado",'Autor'=>"H.P. Lovecraft",'Editorial'=>"Akal",'Idioma'=>"Castellano",'Precio'=>65),
			'Hiperbórea'=>array('Imagen'=>"https://imagessl2.casadellibro.com/a/l/t0/82/9788477027782.jpg",'Título'=>"Hiperbórea",'Autor'=>"Clark Ashton Smith",'Editorial'=>"Valdemar",'Idioma'=>"Castellano",'Precio'=>23.52),
			'Cuentos de un soñador'=>array('Imagen'=>"https://imagessl7.casadellibro.com/a/l/t0/87/9788477028987.jpg",'Título'=>"Cuentos de un soñador",'Autor'=>"Lord Dunsany",'Editorial'=>"Valdemar",'Idioma'=>"Castellano",'Precio'=>27.50)
		);

		if (isset($_POST['login'])) {
			$carrito = array('Cuentos completos'=>0,'H.P. Lovecraft anotado'=>0,'Hiperbórea'=>0,'Cuentos de un soñador'=>0);
			$_SESSION['carro']=$carrito;
			$_SESSION['user']=$_POST['user'];
			$precio=array();
			foreach ($libros as $tipo => $libro) {
				foreach ($libro as $dato => $valor) {
					if ($dato=="Precio") {
						$precio[$tipo]=$valor;
					}
				}
			}
			$_SESSION['precio']=$precio;
			$img=array();
			foreach ($libros as $ind => $libro) {
				foreach ($libro as $dato => $url) {
					if ($dato=="Imagen") {
						$img[$ind]=$url;
					}
				}
			}
			$_SESSION['imagenes']=$img;
		}

	?>

	<header>
		<img src="logo.png" alt="Logo Book A Book">
	</header>

	<div class="bienvenida">
		<h1>Bienvenido/a, <?php echo $_SESSION['user']; ?></h1>
		<br/><br/>
		<blockquote>
			<p style="font-style: italic;">"Siempre imaginé que el Paraíso sería algún tipo de biblioteca"</p>
			<cite>- Jorge Luis Borges</cite>
		</blockquote>
	</div>
	<br/><br/>

	<?php

		echo "<form action='E2_control.php' method='POST'>";
		echo "<div class='productos'>";
			foreach ($libros as $tipo => $libro) {
				echo "<div class='producto'>";
					foreach ($libro as $dato => $valor) {
						if ($dato=='Imagen') {
							echo "<img class='img_hover' src='$valor' height='130' alt='Portada de $tipo'>";
						}
						else {
							if ($dato=='Precio') {
								echo "<p>$dato: <strong>$valor €</strong></p>";
							}
							else {
							echo "<p>$dato: <strong>$valor</strong></p>";
							}
						}
					}
				echo "<br/><button type='submit' name='add' value='$tipo'>Agregar</button>";
				echo "</div>";
			}
		echo "</div><br/>";

		$total=0;
		foreach ($_SESSION['carro'] as $tipo => $cant) {
			if ($cant>0) {
				$precio=($cant*$_SESSION['precio'][$tipo]);
				$total+=$precio;
			}
		}
		$_SESSION['total']=$total;

	
	if ($_SESSION['carro']['Cuentos completos']!=0 || $_SESSION['carro']['H.P. Lovecraft anotado']!=0 || $_SESSION['carro']['Hiperbórea']!=0 || $_SESSION['carro']['Cuentos de un soñador']!=0) {
		echo "<aside><h2>Carro</h2>";
		foreach ($_SESSION as $ind => $producto) {
			if ($ind=="carro") {
				foreach ($producto as $nom => $valor) {
					if ($valor!=0) {
						echo "$nom: $valor <button type='submit' name='del' value='$nom'>Quitar</button><br/><br/>";
					}
				}
			}
		}
		echo "<strong>Total: </strong>".$_SESSION['total']." €<br/><br/>";
		echo "<button type='submit' name='fc'>Finalizar compra</button>";
	}
	echo "</aside>";
	echo "</form>";
	?>
</body>
</html>