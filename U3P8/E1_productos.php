<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 1</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	session_start();

		if (isset($_POST['login'])) {
			$carrito = array('TV'=>0,'Móvil'=>0,'MP4'=>0,'Ratón'=>0,'Alfombrilla'=>0,'USB'=>0);
			$_SESSION['carro']=$carrito;
			$_SESSION['user']=$_POST['user'];
		}

		$productos = array(
			'TV'=>array('Producto'=>"TV",'Descripcion'=>"22 pulgadas",'Precio'=>"210 €"),
			'Móvil'=>array('Producto'=>"Movil",'Descripcion'=>"4g",'Precio'=>"300 €"),
			'MP4'=>array('Producto'=>"MP4",'Descripcion'=>"20Gb",'Precio'=>"13 €"),
			'Ratón'=>array('Producto'=>"Raton",'Descripcion'=>"6000 dpi",'Precio'=>"20 €"),
			'Alfombrilla'=>array('Producto'=>"Alfombrilla",'Descripcion'=>"Negra",'Precio'=>"30 €"),
			'USB'=>array('Producto'=>"USB",'Descripcion'=>"2Gb",'Precio'=>"5 €")
		);

	?>

	<h1>Bienvenid@, <?php echo $_SESSION['user']; ?></h1>

	<?php

		echo "<form action='E1_control.php' method='POST'>";
		echo "<table border='1'><tr>";
			foreach ($productos as $tipo => $producto) {
				echo "<td style='padding: 15px;'>";
					foreach ($producto as $dato => $valor) {
						echo "<p>$dato: <strong>$valor</strong></p>";
					}
				echo "<button type='submit' name='add' value='$tipo'>Agregar</button>";
				echo "</td>";
			}
		echo "</tr></table><br/>";

		
	if ($_SESSION['carro']['TV']!=0 || $_SESSION['carro']['Móvil']!=0 || $_SESSION['carro']['MP4']!=0 || $_SESSION['carro']['Ratón']!=0 || $_SESSION['carro']['Alfombrilla']!=0 || $_SESSION['carro']['USB']!=0) {
		echo "<h2>Carro</h2>";
		foreach ($_SESSION as $ind => $producto) {
			if ($ind=="carro") {
				foreach ($producto as $nom => $valor) {
					if ($valor!=0) {
						echo "$nom: $valor <button type='submit' name='del' value='$nom'>Quitar</button><br/><br/>";
					}
				}
			}
		}
	}
	echo "</form>";
	
	?>
</body>
</html>