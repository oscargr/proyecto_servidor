<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Dancing+Script|Lobster|Permanent+Marker&display=swap');
	</style>
</head>
<body>
	<?php

	session_start();

	?>

	<header>
		<img src="logo.png" alt="Logo Book A Book">
	</header>

	<?php

	echo "<div class='tarifa'>";
	echo "<h1 style='text-align: center;'>Recibo de compra</h1><hr><br/>";
	foreach ($_SESSION as $pos => $arr) {
		if ($pos=="carro") {
			foreach ($arr as $ind => $valor) {
				if ($valor!=0) {
					echo "<p><img  class='thumb' src=".$_SESSION['imagenes'][$ind]." height='130' alt='Portada de $ind'>";
					echo "$ind Cantidad: $valor Precio: ".$_SESSION['precio'][$ind]." €</p><br/><hr><br/>";
				}
			}
		}
	}
	echo "<p class='total'><strong>TOTAL: </strong>".$_SESSION['total']." €</p>";
	echo "<form action='' method='POST'>";
	echo "<p><button type='submit' name='tc'>Terminar compra</button>";
	echo "</form></p>";
	echo "</div>";

	if (isset($_POST['tc'])) {
		session_destroy();
		header("Location:E2_formulario.php");
	}

	?>
</body>
</html>