<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="eform.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Dancing+Script&display=swap');
		.log {
			font-family: 'Dancing Script', cursive;
		}
	</style> 
</head>
<body>
	<div class="log">
		<form action="E2_productos.php" method="POST">
			<h1>Bienvenid@ a</h1>
			<img src="logo.png" alt="Logo Book a book">
			<br/>
			Introduce tu nombre: <br/><input type="text" name="user"><br/>
			<input type="submit" name="login" value="Log In" style="cursor: pointer;">
		</form>
	</div>
</body>
</html>