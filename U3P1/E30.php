<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 30</title>
	<meta charset="utf-8">
</head>
<body>
	<form action="" method="POST">
		<fieldset>
			<legend>Cálculo de números perfectos</legend>
			<br/>Introduce el número para comprobar si es perfecto:<br/><br/>
			<strong>Número: </strong><input type="text" name="num" id="num">
			<input type="submit" name="np" value="Comprobar">
		</fieldset>
	</form>

	<?php

	if (isset($_POST['np'])) {
		$n = $_POST['num'];
		$sum=0;
		for ($i=1; $i<=round($n/2); $i++) {
			if ($n%$i==0) {
				$sum+=$i;
			}
		}

		if ($sum==$n) {
			echo "<br/><br/><strong>$n es un número perfecto</strong>";
		}
		else {
			echo "<br/><br/><strong>$n NO es un número perfecto</strong>";
		}
	}

	?>
</body>
</html>