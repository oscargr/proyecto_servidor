<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 17</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$n1=rand(0,100);
	$n2=rand(0,100);

	echo "Número 1: $n1<br/><br/>Número 2: $n2<br/><br/>";

	if ($n1>$n2) {
		$mayor=$n1;
	}
	else {
		$mayor=$n2;
	}

	if ($mayor%2==0) {
		echo "$mayor es el mayor y es un número par";
	}
	else {
		echo "$mayor es el mayor y es un número impar";
	}

	?>
</body>
</html>