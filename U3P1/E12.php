<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 12</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$v="Esto es una prueba de PHP<br/><br/>";

	echo "$v";

	print($v);

	?>

	<p><strong>print</strong> imprime una cadena, <strong>echo</strong> puede imprimir más de una separadas por coma.

	<strong>print</strong> devuelve un valor int que según la documentación siempre es 1, por lo que puede ser utilizado en expresiones mientras que <strong>echo</strong> es tipo void, no hay valor devuelto y no puede ser utilizado en expresiones.</p>
</body>
</html>