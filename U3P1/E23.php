<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 23</title>
	<meta charset="utf-8">
	<meta lang="es">
</head>
<body>
	<?php

	$num=0;

	do {
		echo $num." ";
		$num++;
	}
	while ($num<=9);

	echo "<p><strong>Bucle finalizado</strong></p>";

	?>
</body>
</html>