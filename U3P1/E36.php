<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 36</title>
	<meta charset="utf-8">
</head>
<body>

	<form action="" method="POST">
		A: <input type="text" name="n1" id="n1">
		B: <input type="text" name="n2" id="n2">
		<br/>
		<input type="submit" name="sum" value="Sumar">
		<input type="submit" name="rest" value="Restar">
		<input type="submit" name="mult" value="Multiplicar">
		<input type="submit" name="div" value="Dividir">
	</form>

	<?php

		switch (true) {
			case isset($_POST['sum']):
			$n1 = $_POST['n1'];
			$n2 = $_POST['n2'];
			$sum = $n1+$n2;
			echo "La suma de $n1 + $n2 es $sum";
			break;
			case isset($_POST['rest']):
			$n1 = $_POST['n1'];
			$n2 = $_POST['n2'];
			$rest = $n1-$n2;
			echo "La resta de $n1 - $n2 es $rest";
			break;
			case isset($_POST['mult']):
			$n1 = $_POST['n1'];
			$n2 = $_POST['n2'];
			$mult = $n1*$n2;
			echo "La multiplicación de $n1 * $n2 es $mult";
			break;
			case isset($_POST['div']):
			$n1 = $_POST['n1'];
			$n2 = $_POST['n2'];
			$div = $n1/$n2;
			echo "La división de $n1 / $n2 es $div";
			break;
		}

	?>
</body>
</html>