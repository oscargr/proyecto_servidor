<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 28</title>
	<meta charset="utf-8">
</head>
<body>

	<form action="" method="POST">
		<fieldset>
			<legend>Formulario</legend>
			Introduzca cinco números: <br/><br/>
			Número 1: <input type="text" name="n1" id="n1"><br/>
			Número 2: <input type="text" name="n2" id="n2"><br/>
			Número 3: <input type="text" name="n3" id="n3"><br/>
			Número 4: <input type="text" name="n4" id="n4"><br/>
			Número 5: <input type="text" name="n5" id="n5"><br/>
			<input type="submit" name="med" value="Enviar">
		</fieldset>
	</form>

	<?php
	if (isset($_POST['med'])) {
	$n1=$_POST['n1'];
	$n2=$_POST['n2'];
	$n3=$_POST['n3'];
	$n4=$_POST['n4'];
	$n5=$_POST['n5'];

	$vec=array($n1,$n2,$n3,$n4,$n5);

	echo "Números enviados:<br/><br/>$n1 $n2 $n3 $n4 $n5<br/><br/>";

	$mayor=0;
	$menor=999999;
	for ($i=0; $i<=4; $i++) {
		if ($vec[$i]>$mayor) {
			$mayor=$vec[$i];
		}
		if ($vec[$i]<$menor) {
			$menor=$vec[$i];
		}
	}

	$med=($n1+$n2+$n3+$n4+$n5)/5;

	echo "El mínimo es: $menor<br/>El máximo es: $mayor<br/>La media es: $med";
	}

	?>
</body>
</html>