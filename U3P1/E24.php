<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 24</title>
	<meta charset="utf-8">
	<meta lang="es">
</head>
<body>
	<?php

	$num=50;

	do {
		echo $num." ";
		$num--;
	}
	while ($num>=40);

	echo "<p><strong>Bucle finalizado</strong></p>";

	$contador=1;

	do {
		echo ($contador*2)." ";
		$contador++;
	}
	while ($contador<=5);

	echo "<p><strong>Bucle finalizado</strong></p>";

	?>
</body>
</html>