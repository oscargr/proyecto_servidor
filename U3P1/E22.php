<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 22</title>
	<meta charset="utf-8">
	<meta lang="es">
</head>
<body>
	<?php

	$num=50;

	while ($num>=40) {
		echo $num." ";
		$num--;
	}

	echo "<p><strong>Bucle finalizado</strong></p>";

	$contador=1;

	while ($contador<=5) {
		echo ($contador*2)." ";
		$contador++;
	}

	echo "<p><strong>Bucle finalizado</strong></p>";

	?>
</body>
</html>