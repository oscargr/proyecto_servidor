<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 34</title>
	<meta charset="utf-8">
	<style type="text/css">
		form fieldset {
			width: 20%;
		}
		table {
			border-color: rgba(63,109,78,1);
		}
		.void {
			background-color: rgba(29,26,47,1);
			padding: 5px;
			border-radius: 10px;
		}
		.ds {
			background-color: rgba(139,212,80,1);
			padding: 10px;
			font-size: 18px;
		}
		.cal {
			background-color: rgba(115,79,154,1);
			font-size: 24px;
			padding: 15px;
			text-shadow: 2px 2px 2px #8bd450;
		}
		.dias {
			background-color: rgba(150,95,212,1);
			text-align: center;
			padding: 5px;
		}
		.dias:hover {
			background-color: rgba(139,212,80,1);
		}
	</style>
</head>
<body>
	<form action="" method="POST">
		<fieldset>
			<legend>Vista de calendario</legend>
			<strong>Introducir el número de mes: </strong>
			<input type="text" name="mes"><br/><br/>
			<strong>Introducir el año: </strong>
			<input type="text" name="anio"><br/><br/>
			<input type="submit" name="en" value="Generar calendario">
		</fieldset>
	</form>

	<?php  

	if (isset($_POST['en'])) {
		$mes = $_POST['mes'];
		$anio = $_POST['anio'];
		$date = "$anio-$mes-01";
		$dia = date('w', strtotime($date))+7;
		$ndias = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
		$meses=array(1=>"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto","Septiembre", "Octubre", "Noviembre", "Diciembre");
	?>
	<br/><br/>
	<table border="5">
	<tr>
		<th colspan="7" class="cal">Calendario <?php echo $meses[$mes]." ".$anio; ?></th>
	</tr>
	<tr>
		<th class="ds">Lunes</th><th class="ds">Martes</th><th class="ds">Miércoles</th><th class="ds">Jueves</th><th class="ds">Viernes</th><th class="ds">Sábado</th><th class="ds">Domingo</th>
	</tr>

	<?php

	if ($dia==7) {
		$ultimodia=$dia+$ndias;
		for ($i=1; $i<=42; $i++) {
			if ($i==$dia) {
				$day = 1;
			}
			if ($i<$dia || $i>=$ultimodia) {
				echo "<td class='void'>&nbsp;</td>";
			}
			else {
				echo "<td class='dias'>$day</td>";
				$day++;
			}
			if($i%7==0) {
				echo "</tr><tr>\n";
			}
		}
	}
	else {
		$dia-=7;
		$ultimodia=$dia+$ndias;
		for ($i=1; $i<=35; $i++) {
			if ($i==$dia) {
				$day = 1;
			}
			if ($i<$dia || $i>=$ultimodia) {
				echo "<td class='void'>&nbsp;</td>";
			}
			else {
				echo "<td class='dias'>$day</td>";
				$day++;
			}
			if($i%7==0) {
				echo "</tr><tr>\n";
			}
		}
	}

	}

	?>
</body>
</html>