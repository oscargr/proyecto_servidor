<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 35</title>
	<meta charset="utf-8">
</head>
<body>

	<?php 

	if (!isset($_POST['sum'])){

	?>

	<form action="" method="POST">
		A: <input type="text" name="n1" id="n1">
		B: <input type="text" name="n2" id="n2">
		<input type="submit" name="sum" value="Sumar">
	</form>

	<?php

	}
	
	else {
		$n1 = $_POST['n1'];
		$n2 = $_POST['n2'];
		$sum = $n1+$n2;
		echo "La suma de $n1 + $n2 es $sum";
	}

	?>
</body>
</html>