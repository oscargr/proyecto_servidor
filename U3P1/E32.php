<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 32</title>
	<meta charset="utf-8">
</head>
<body>

	<?php 

	if (!isset($_POST['pares']) && !isset($_POST['impares'])){

	?>	

	<h2>Introduce un número</h2>

	<form action="" method="POST">
		Número: <input type="text" name="numero" id="numero">
		<br/><br/>
		<input type="submit" name="pares" id="pares" value="Pares">
		<input type="submit" name="impares" id="impares" value="Impares">
		<!-- De los input submit el POST coge el valor de name, por lo que debe ser diferente, en el caso de los radio coge el valor de value -->
		<br/><br/>
	</form>

	<?php
    }
	else
    {
	//var_dump($_POST); Sirve para saber el tipo de variable y el valor de la variable entre paréntesis

		if (isset($_POST['pares'])) {
			$n=$_POST['numero'];//Guarda el post en la variable "n"
			echo "Números pares de 0 a $n<br/><br/>";
			for ($i=0; $i<=$n; $i++) {
				if ($i%2==0) {
					echo "$i ";
				}
			}
		}

		if (isset($_POST['impares'])) {
			$n=$_POST['numero'];
			echo "Números impares de 0 a $n<br/><br/>";
			for ($j=0; $j<=$n; $j++) {
				if ($j%2!=0) {
					echo "$j ";
				}
			}
		}

	}
	?>
</body>
</html>