<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 31</title>
	<meta charset="utf-8">
</head>
<body>
	<form action="" method="POST">
		<fieldset>
			<legend>Cálculo de números primos</legend>
			<br/>Introduce el número para comprobar si es primo:<br/><br/>
			<strong>Número: </strong><input type="text" name="num" id="num">
			<input type="submit" name="np" value="Comprobar">
		</fieldset>
	</form>

	<?php

	if (isset($_POST['np'])) {
		$n = $_POST['num'];
		$ndiv = 0;
		for ($i=1; $i<=round($n/2); $i++) {
			if ($n%$i==0) {
				$ndiv++;
			}
		}
		if ($ndiv==2 || $ndiv==1) {
			echo "<br/><br/><strong>$n es un número primo</strong>";
		}
		else {
			echo "<br/><br/><strong>$n NO es un número primo</strong>";
		}
	}

	?>
</body>
</html>