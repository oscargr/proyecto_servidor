<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 18</title>
	<meta charset="utf-8">
</head>
<body>

	<form action="" method="POST">
		<fieldset>
			<legend>Formulario de prueba con método POST</legend>
			Introducir un número:
			<input type="text" name="n">
			<br/>
			<input type="submit" name="en1" value="Enviar">
		</fieldset>
	</form>

	<br/><br/>

	<form action="" method="GET">
		<fieldset>
			<legend>Formulario de prueba con método GET</legend>
			Introducir una palabra:
			<input type="text" name="p">
			<br/>
			<input type="submit" name="en2" value="Enviar">
		</fieldset>
	</form>

	<br/><br/>

	<?php

	if (isset($_POST['en1'])) {
		if (!empty($_POST['n'])) {
		$num=$_POST['n'];
		echo "<p>'isset' es una función de manejo de variables, se utiliza en condicionales y devuelve TRUE si la variable está definida y tiene un valor distinto de NULL y false de lo contrario.</p>
			<p>Por tanto, la función 'isset' es la condición y a ésta se le pasa lo que se ha guardado en la variable del método POST o GET.</p>
			<p>Al pulsar enviar en el primer formulario se ha utilizado el método POST y se ha guardado el número en una variable, siendo este número <strong>$num</strong></p><br/><br/>";
		}
		else  {
		echo "Si aparece este mensaje la función 'empty' está controlando que el el número del formulario se haya introducido<br/><br/>";
		}
	}
	

	if (!empty($_GET['p']) && isset($_GET['en2'])) {
		echo "Si aparece este mensaje ha enviado correctamente la palabra y no es una variable vacía, en la URL se pueden ver los valores de los input hechos en el formulario que usa el método GET, esa es la principal diferencia con el método POST<br/><br/>";
	}

	?>
</body>
</html>