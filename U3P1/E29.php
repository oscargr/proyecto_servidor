<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 29</title>
	<meta charset="utf-8">
</head>
<body>

	<form action="" method="POST">
		<fieldset>
			<legend>Formulario</legend>
			Introducir dos números para calcular la potencia:<br/><br/>
			<strong>Base: </strong><input type="text" name="base" id="base"><br/><br/>
			<strong>Exponente: </strong><input type="text" name="exponente" id="exponente"><br/><br/>
			<input type="submit" name="pot" value="Elevar">
		</fieldset>
	</form>

	<?php

	if (isset($_POST['pot'])) {
		$base=$_POST['base'];
		$exponente=$_POST['exponente'];
		$pot=pow($base, $exponente);
		echo "<br/><br/><strong>$base elvado a $exponente es $pot</strong>";
	}

	?>
</body>
</html>