<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 20</title>
	<meta charset="utf-8">
	<style type="text/css">
		body {
			background-color: lightgrey;
		}

		h2 {
			text-align: center;
		}

		form fieldset {
			border: 2px solid #cc99ff;
			padding: 15px;
			background-color: #ccccff;
		}

		legend {
			border: 2px solid #cc99ff;
			background-color: white;
			padding: 5px;
		}

		.ec {
			font-size: 24px;
			font-weight: bold;
		}

		.coe {
			width: 5%;
		}

		.caja {
			text-align: right;
		}
	</style>
</head>
<body>

	<?php 

	if (!isset($_POST['calc'])){

	?>

	<h2>ECUACIÓN DE SEGUNDO GRADO (FORMULARIO)</h2>

	<form action="" method="POST" >
		<fieldset>
			<legend>Formulario</legend>

			Dada la ecuación de segundo grado <span class="ec">a*x<sup>2</sup> + b*x + c = 0</span>. escriba los valores de los tres coeficientes y resolveré la ecuación:
			<br/><br/>

			<strong>a: </strong><input type="text" name="a" id="a" class="coe"><br/><br/>
			<strong>b: </strong><input type="text" name="b" id="b" class="coe"><br/><br/>
			<strong>c: </strong><input type="text" name="c" id="c" class="coe"><br/><br/>

			<div class="caja">
				<input type="submit" name="calc" value="Calcular">
				<input type="reset" name="res" value="Borrar">
			</div>

		</fieldset>
	</form>

	<?php
	}

	else {

		$a=$_POST['a'];
		$b=$_POST['b'];
		$c=$_POST['c'];

		$det=(pow($b, 2)) - (4*$a*$c);

		if ($det<0) {
			echo "La ecuación no tiene ninguna solución";
		}
		elseif ($det==0) {
			$sol=($b*-1)/(2*$a);
			echo "La solución a la ecuación es $sol";
		}
		else {
			$sol1=(($b*-1)+(sqrt(pow($b, 2)-(4*$a*$c))))/(2*$a);
			$sol2=(($b*-1)-(sqrt(pow($b, 2)-(4*$a*$c))))/(2*$a);
			echo "La ecuación tiene 2 soluciones:<br/>Solución 1: $sol1<br/>Solución 2: $sol2";
		}

	}


	?>
</body>
</html>