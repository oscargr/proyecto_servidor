<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ejercicio 13 </title>
  <style>
    .error {
      color: red;
    }
  </style>
</head>

<body>
  <h1>Ejercicio 13</h1>
  <h2>FORMULARIO PHP</h2>
  <?php
  if (isset($_POST["enviar"])) {
    $nameErr = funcion_validar_nombre($_POST["nombre"]);
    $apellidoErr = funcion_validar_apellido($_POST["apellido"]);
    $dniErr = funcion_validar_dni($_POST["dni"]);
    $telefonoErr = funcion_validar_telefono($_POST["telefono"]);
    $emailErr =  funcion_validar_email($_POST["emailUser"]);
    $urlErr = funcion_validar_url($_POST["url"]);

    if (isset($_POST["sexo"])) {
      $sexo = $_POST["sexo"];
      $sexoErr = "";
    } else {
      $sexo = "";
      $sexoErr = "Seleccionar un valor";
    }

    if ($nameErr || $apellidoErr ||$dniErr || $telefonoErr || $emailErr || $urlErr || $sexoErr) {
      echo '<span class="error">LOS CAMPOS CON * SON OBLIGATORIOS</span>';
    }
  }
  ?>

  <br />
  <form action="" method="POST">
    <label for="nombre">Name:</label><br>
    <input type="text" name="nombre" value="<?php if (isset($_POST["enviar"])) echo $_POST["nombre"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"])) echo $nameErr ?></span><br /><br>

    <label for="nombre">Apellido:</label><br>
    <input type="text" name="apellido" value="<?php if (isset($_POST["enviar"])) echo $_POST["apellido"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"])) echo $nameErr ?></span><br /><br>

    <label for="dni">DNI:</label><br>
    <input type="text" name="dni" value="<?php if (isset($_POST["enviar"])) echo $_POST["dni"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"])) echo $dniErr ?></span><br /><br>

    <label for="telefono">Teléfono:</label><br>
    <input type="text" name="telefono" value="<?php if (isset($_POST["enviar"])) echo $_POST["telefono"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"])) echo $telefonoErr ?></span><br/><br>

    <label for="emailUser">Email:</label><br>
    <input type="text" name="emailUser" value="<?php if (isset($_POST["enviar"])) echo $_POST["emailUser"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"]))  echo $emailErr ?></span><br /><br>

    <label for="emailUser">Website:</label><br>
    <input type="text" name="url" value="<?php if (isset($_POST["enviar"])) echo $_POST["url"] ?>" required>
    <span class="error">*<?php if (isset($_POST["enviar"]))  echo $urlErr ?></span><br /><br>

    <label for="comment">Comment:</label><br>
    <textarea rows="6" cols="35" name="comment" value="" required><?php if (isset($_POST["enviar"])) echo $_POST["comment"] ?></textarea>
    <br/><br>

    <label for="ciudad">Ciudad</label>
    <select name="ciudad">
      <option value="value1">Avilés</option>
      <option value="value2" selected>Gijón</option>
      <option value="value3">Oviedo</option>
    </select>

    <br><br>
    Sexo:
    <input type="radio" name="sexo" <?php if (isset($sexo) && $sexo == "mujer") echo "checked"; ?> value="mujer" checked>
    Mujer
    <input type="radio" name="sexo" <?php if (isset($sexo) && $sexo == "hombre") echo "checked"; ?> value="hombre">
    Hombre
    <span class="error">*<?php if (isset($_POST["enviar"])) echo $sexoErr; ?> </span>
    <br><br>

    <input type="submit" name="enviar" value="ENVIAR">

  </form>
  <br>
  <?php
  if (isset($_POST["enviar"])) {
    echo "<h2>Datos introducidos:</h2>  ";
    if (empty($nameErr) && empty($emailErr) && empty($dniErr) && empty($telefonoErr) && empty($urlErr) && empty($apellidoErr)) {
      echo "Nombre: " . $_POST["nombre"] . "<br/>";
      echo "Apellido: ".$_POST["apellido"] . "<br/>";
      echo "DNI: ". $_POST["dni"] . "<br/>";
      echo "Telefono: ". $_POST["telefono"] . "<br/>";
      echo "E-Mail: " . $_POST["emailUser"] . "<br/>";
      echo "Website :" . $_POST["url"] . "<br/>";
      echo "Comentario: " . $_POST["comment"] . "<br/>";
      echo "Sexo: " . $_POST["sexo"] . "<br/>";
    }
  }


  function funcion_validar_url($web)
  {
    if (empty($web)) {
      return " Introducir una dirección URL";
    } else {
      if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|](\.)[a-z]{2}/i", $web)) {
        return " Formato de URL inválido";
      }
    }
  }

  function funcion_validar_email($mail)
  {
    if (empty($mail)) {
      return " Introducir un correo";
    } else {
      if (!preg_match("/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/", $mail)) {
        return " Formato de correo inválido";
      }
    }
  }

  function funcion_validar_nombre($nombre)
  {
    if (empty($nombre)) {
      return " Introducir un nombre";
    } else {
      if (!preg_match("/^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/", $nombre)) {
        return " Solo son válidos letras y espacios en blanco";
      }
    }
  }
  function funcion_validar_apellido($apellido)
  {
    if (empty($apellido)) {
      return " Introducir un apellido";
    } else {
      if (!preg_match("/^[A-Za-zÁÉÍÓÚáéíóúñÑ ]+$/", $apellido)) {
        return " Solo son válidos letras y espacios en blanco";
      }
    }
  }
  function funcion_validar_dni($dni){
     if (empty($dni)) {
      return " Introducir un dni";
    } else {
      if(!preg_match('/[0-9]{8}[A-Z]/', $dni)){
        return "El formato del dni es inválido";
      }

  }
}

function funcion_validar_telefono($telefono){
  if (empty($telefono)) {
      return " Introducir un telefono";
    } else {
      if(!preg_match('/[6-9][0-9]{8}/', $telefono)){
        return "El formato del telefono es inválido";
      }

}
 } 
  ?>


</body>
</html>