<!DOCTYPE html>
<html>
<head>
	<title>Prueba de control de errores</title>
</head>
<body>
	<h2>Esta página utiliza tres formas distintas de manejo de errores (que las contraseñas sean iguales se controlar con un trigger_error(), si se hace un echo de una variable indefinida mail, se controla con una error_function() y el error de insert se controla con die()</h2>
		<div class="registro">
			<div>
				<table>
					<tr>
						<th colspan="2">
							<h2>Registro</h2>
						</th>
					</tr>
					<form action="" method="POST" name="formReg" onsubmit="return validar()">
						<tr>
							<td class="datosform">Usuario: </td><td><input type="text" name="usu" required="required"></td>
						</tr>
						<tr>
							<td class="datosform">Correo electrónico: </td><td><input type="text" name="mail"></td>
						</tr>
						<tr>
							<td class="datosform">Contraseña: </td><td><input type="password" name="npass" required="required"></td>
						</tr>
						<tr>
							<td class="datosform">Confirma contraseña: </td><td><input type="password" name="renpass" required="required" onblur="validarcontra()"><span id="avisocontra"></span></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="reg" value="Crear cuenta"></td>
						</tr>
					</form>
				</table>
			</div>
		</div>
</body>
</html>

<?php

if (isset($_POST['reg'])) {
	$conexion=mysqli_connect('localhost', 'AccesoTienda', 'AccesoTienda', 'tienda');
	if (mysqli_connect_errno()) {
		printf("Conexión fallida %s\n", mysqli_connect_error());
		exit();
	}

	function errorCon($errno, $errmsg) {
		echo "<p><strong>Error: </strong> [$errno] $errmsg </p>";
	}

	set_error_handler("errorCon",E_USER_WARNING);

	if ($_POST['npass']===$_POST['renpass']) {
		echo "Contraseña correcta";
	}
	else {
		trigger_error("Las contraseñas no son iguales, vuelva a introducirlas",E_USER_WARNING);
	}

	function errorVar($errno, $errmsg) {
		echo "<p><strong>Error:</strong> [$errno] $errmsg</p>";
	}

	set_error_handler("errorVar");

	echo $mail;

	$user=$_POST['usu'];
	$mail=$_POST['mail'];
	$contra=$_POST['renpass'];
	$contraHash=hash_hmac("sha512", $contra, "tiendaonline", FALSE);
	$sql="INSERT INTO usuarios (idusuario,usuario,email,password,rol) VALUES (NULL,'$user','$mail','$contraHash','Cliente');";
	if (mysqli_query($conexion, $sql)) {
		echo "Se ha registrado el usuario con éxito";
	}
	else {
		die("<p>El usuario que intenta registrarle en la base de datos no tiene permisos de escritura, contacte con el administrador de la página.</p>");
	}
	mysqli_close($conexion);

}

?>