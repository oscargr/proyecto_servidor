<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ejercicio 4</title>
</head>
<body>
	<h2>Películas ordenadas por año</h2>
	<table border="1">
		<tr>
			<th>Película más nueva</th>
			<th>Película más antigua</th>
		</tr>
		<tr>
		<?php
		$conexion = mysqli_connect('localhost', 'root', '', 'video');
		if (mysqli_connect_errno()) {
			printf("Conexión fallida %s\n", mysqli_connect_error());
			exit();
		}
		$sql="SELECT title FROM peliculas WHERE year=(SELECT MAX(year) FROM peliculas);";
		$result = mysqli_query ($conexion, $sql);

			if(mysqli_num_rows($result) > 0) {
				while ($registro = mysqli_fetch_row($result)) {
		?>
			<th><?php echo $registro[0]; ?></th>
		<?php
	        	}
	        }

	    $sql2="SELECT title FROM peliculas WHERE year=(SELECT MIN(year) FROM peliculas);";
	     $result2 = mysqli_query ($conexion, $sql2);

			if(mysqli_num_rows($result2) > 0) {
				while ($registro2 = mysqli_fetch_row($result2)) {
	?>
			<th><?php echo $registro2[0]; ?></th>
	<?php
	        	}
	        }
		?>
		</tr>
	</table>

	<?php
	$sql="SELECT title,year FROM peliculas;";
	$result = mysqli_query ($conexion, $sql);

			if(mysqli_num_rows($result) > 0) {
				while ($registro = mysqli_fetch_row($result)) {
					$anio=$registro[1];
					$registro[1]=($registro[1]-1900)*2;
		?>
			<p><?php echo $registro[0]; ?></p>
			<?php echo $anio; ?>
			<svg width="250" height="20">
				<rect style="fill: silver" x="0" y="0" height="20" width="<?php echo $registro[1]; ?>"></rect>
			</svg>

		<?php
	        	}
	        }
	        mysqli_close($conexion);
	?>
</body>
</html>