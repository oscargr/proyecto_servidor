<!DOCTYPE html>
<html lang="es">
<head>
	<title>Tienda de libros</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="icon" href="icono.ico" type="image/ico" sizes="16x16">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Lekton|Special+Elite&display=swap');
	</style>
</head>
<body>
	<div class="cabecera">
		<div class="logoindex">
			<img src="logo.png" width="250">
		</div>
		<div class="nombreindex">
			<h1>Miskatonic Bookshop</h1>
		</div>
	</div>
	<div class="botonAR">
		<input type="button" name="boton" value="Acceder o registrarse" onclick="mostrarOcultar()">
	</div>
	<div class="contenido" id="accreg">
		<div class="acceso">
			<div>
				<table>
					<tr>
						<th colspan="2">
							<h2>Acceso</h2>
						</th>
					</tr>
				<form action="" method="POST">
					<tr>
						<td class="datosform">Usuario o email:</td><td><input type="text" name="usu-mail" required="required"></td>
					</tr>
					<tr>
						<td class="datosform">Contraseña:</td><td><input type="password" name="pass" required="required"></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="login" value="Entrar"></td>
					</tr>
				</form>
				</table>
			</div>
		</div>
		<div class="registro">
			<div>
				<table>
					<tr>
						<th colspan="2">
							<h2>Registro</h2>
						</th>
					</tr>
					<form action="" method="POST" name="formReg" onsubmit="return validar()">
						<tr>
							<td class="datosform">Usuario: </td><td><input type="text" name="usu" required="required"></td>
						</tr>
						<tr>
							<td class="datosform">Correo electrónico: </td><td><input type="email" name="mail" required="required"></td>
						</tr>
						<tr>
							<td class="datosform">Contraseña: </td><td><input type="password" name="npass" required="required"></td>
						</tr>
						<tr>
							<td class="datosform">Confirma contraseña: </td><td><input type="password" name="renpass" required="required" onblur="validarcontra()"><span id="avisocontra"></span></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="reg" value="Crear cuenta"></td>
						</tr>
					</form>
				</table>
			</div>
		</div>
	</div>
	<div class="galeria">
		
		<div class="fotos difuminado">
			<img src="estante.jpg" alt="" style="width: 100%;">
		</div>

		<div class="fotos difuminado">
			<img src="kindle.jpg" alt="" style="width: 100%;">
		</div>

		<div class="fotos difuminado">
			<img src="ilus1.jpg" alt="" style="width: 100%;">
		</div>

		<div class="fotos difuminado">
			<img src="ebook.jpg" alt="" style="width: 100%;">
		</div>

		<div class="fotos difuminado">
			<img src="ilus2.jpg" alt="" style="width: 100%;">
		</div>

	</div>
	<div id="modalC" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalC').style.display='none'" class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p>Las contraseñas no coinciden, por favor vuelva a introducir la contraseña</p>
    		</div>
    	</div>
    </div>
	<div class="piepagina">
		<div>
			<table>
				<tr>
					<th colspan="3">
						<h2>Contacto</h2>
					</th>
				</tr>
				<tr>
					<td>
						<img src="fb.png" width="64" alt="Logo de contacto por Facebook">
					</td>
					<td>
						<img src="ig.png" width="64" alt="Logo de contacto por Instagram">
					</td>
					<td>
						<img src="twitter.png" width="64" alt="Logo de contacto por Twitter">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						2020 &copy; Todos los derechos reservados
					</td>
				</tr>
			</table>
		</div>
	</div>

	<?php

	if (isset($_POST['login'])) {
		$conexion=mysqli_connect('localhost', 'AccesoTienda', 'AccesoTienda', 'tienda');
		if (mysqli_connect_errno()) {
		    printf("Conexión fallida %s\n", mysqli_connect_error());
		    exit();
		}

		$user=$_POST['usu-mail'];
		$pass=$_POST['pass'];
		$contraHash=hash_hmac("sha512", $pass, "tiendaonline", FALSE);
		$sql = "SELECT usuario,rol,idusuario FROM usuarios WHERE usuario = '$user' AND password = '$contraHash' OR email = '$user' AND password = '$contraHash';";
		$result = mysqli_query ($conexion, $sql);
		if(mysqli_num_rows($result) > 0) {
			while ($registro = mysqli_fetch_row($result)) {
				$user=$registro[0];
                $rol=$registro[1];
                $id=$registro[2];
        	}
        	
	        session_start();

	     	$_SESSION['usuario']="$user";
	        $_SESSION['rol']="$rol";
	        $_SESSION['id']="$id";

	        header("Location: inicio.php");
     
        	exit();
		}
		else {
			$mensajeaccesoincorrecto = "<p>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
	?>

	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeaccesoincorrecto; ?></p>
    		</div>
    	</div>
    </div>

	<?php
		}
		mysqli_close($conexion);

	}

	if (isset($_POST['reg'])) {
		$conexion=mysqli_connect('localhost', 'RegistroTienda', 'RegistroTienda', 'tienda');
		if (mysqli_connect_errno()) {
		    printf("Conexión fallida %s\n", mysqli_connect_error());
		    exit();
		}

		$user=$_POST['usu'];
		$mail=$_POST['mail'];
		$contra=$_POST['renpass'];
		$contraHash=hash_hmac("sha512", $contra, "tiendaonline", FALSE);
		$sql="INSERT INTO usuarios (idusuario,usuario,email,password,rol) VALUES (NULL,'$user','$mail','$contraHash','Cliente');";
		if (mysqli_query($conexion, $sql)) {
			$mensajeregistro="Se ha registrado el usuario con éxito";
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php		
		}
		else {
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
		mysqli_close($conexion);

	}

	?>

	<script>
		var indiceFoto=0;
		mostrarFoto();

		function mostrarFoto() {
			var foto = document.getElementsByClassName('fotos');
			for (i=0; i<foto.length; i++) {
				foto[i].style.display="none";
			}
			indiceFoto++;
			if(indiceFoto>foto.length) {
				indiceFoto=1;
			}
			foto[indiceFoto-1].style.display="block";
			setTimeout(mostrarFoto,9000);
		}

		function mostrarOcultar() {
			var formulario = document.getElementById('accreg');
			if (formulario.style.display=="flex") {
				formulario.style.display="none";
			}
			else {
				formulario.style.display="flex";
			}
		}

		var modal = document.getElementById('modalB');

		window.onclick = function(event) {
		  if (event.target == modal) {
		    modal.style.display = "none";
		  }
		}

		function validar() {
			if (!validarcontra()) {
				return false;
			}
			else {
				return true;
			}
		}

		function validarcontra() {
			var pass = document.formReg.npass;
			var rpass = document.formReg.renpass;
			var aviso = document.getElementById('avisocontra');

			if (rpass.value===pass.value) {
				rpass.style.border="2px outset green";
				aviso.innerHTML=" &check;";
				return true;
			}
			else {
				rpass.style.border="2px outset red";
				aviso.innerHTML=" &cross;";
				document.getElementById('modalC').style.display="block";
				return false;
			}
		}
	</script>
</body>
</html>