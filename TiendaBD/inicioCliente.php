<?php

ob_start();

?>	
	<div class="main">
		<div class="menu">
			<form action="" method="POST">
				<input type="submit" name="historial" value="Ver historial de compra">
				<input type="submit" name="catalogo" value="Ver el catálogo">
			</form>
		</div>
	</div>
	<p style="text-align: center;">Nuesto cátalogo disponible actualmente, haga scroll en los libros para moverse</p>
	<div class="librosDispo" id="librosDispo" >
<?php

			$conexion=mysqli_connect('localhost', 'ClienteTienda', 'ClienteTienda', 'tienda');
			if (mysqli_connect_errno()) {
			    printf("Conexión fallida %s\n", mysqli_connect_error());
			    exit();
			}

			$sql="SELECT * FROM libros";
			$result = mysqli_query ($conexion, $sql);
			$filas=mysqli_num_rows($result);
			if ($filas>0) {
				while ($registro = mysqli_fetch_row($result)) {
					$_SESSION['imagenes'][$registro[0]]=$registro[7];
					$_SESSION['precios'][$registro[0]]=$registro[5];
					$_SESSION['titulos'][$registro[0]]=$registro[1];
				}
			}

foreach ($_SESSION['imagenes'] as $idart => $src) {
	
?>

	<div class="portada">
		<img height="250" src=<?php echo $src; ?>>
		<div class="titulo">
			<p><?php echo $_SESSION['titulos'][$idart]; ?><br><?php echo $_SESSION['precios'][$idart] ?>€</p>
		</div>
	</div>

<?php


}

?>

</div>

<script>
	(function() {
	    function scrollHorizontally(e) {
	        e = window.event || e;
	        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
	        document.getElementById('librosDispo').scrollLeft -= (delta*40); // Multiplied by 40
	        e.preventDefault();
	    }
	    if (document.getElementById('librosDispo').addEventListener) {
	        // IE9, Chrome, Safari, Opera
	        document.getElementById('librosDispo').addEventListener("mousewheel", scrollHorizontally, false);
	        // Firefox
	        document.getElementById('librosDispo').addEventListener("DOMMouseScroll", scrollHorizontally, false);
	    } else {
	        // IE 6/7/8
	        document.getElementById('librosDispo').attachEvent("onmousewheel", scrollHorizontally);
	    }
	})();
</script>

	<?php

	if (isset($_POST['catalogo'])) {
		header("Location:catalogo.php");
	}

	if (isset($_POST['historial'])){
		header("Location:historial.php");
	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");

	}

	ob_end_flush();