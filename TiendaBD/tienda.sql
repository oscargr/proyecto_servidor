-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2020 a las 13:44:01
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idarticulo` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` int(6) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idarticulo`, `idusuario`, `fecha`, `cantidad`, `precio`) VALUES
(1, 1, '2020-01-27 09:58:50', 1, 32),
(2, 1, '2020-01-27 09:58:50', 1, 65),
(3, 1, '2020-01-27 09:58:50', 1, 24),
(4, 1, '2020-01-27 09:58:50', 1, 7.95);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `idarticulo` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `autor` varchar(50) NOT NULL,
  `editorial` varchar(300) NOT NULL,
  `idioma` varchar(150) NOT NULL,
  `precio` float NOT NULL,
  `resumen` varchar(1000) NOT NULL,
  `imagen` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`idarticulo`, `titulo`, `autor`, `editorial`, `idioma`, `precio`, `resumen`, `imagen`) VALUES
(1, 'Cuentos completos', 'Jorge Luis Borges', 'Lumen', 'Castellano', 32, 'Poeta, ensayista y narrador, Borges es una de las figuras primordiales de la literatura universal. Ahora, por primera vez, se reÃºnen en este volumen todos sus cuentos, uno de los legados mÃ¡s influyentes y deslumbrantes de la literatura occidental. El universo borgiano, con sus espejos, laberintos, tigres, bibliotecas, gauchos, o mÃ¡scaras, es ya uno de los paisajes fundamentales del siglo XX. En este libro se encuentran obras maestras como El jardÃ­n de los senderos que se bifurcan, Pierre Menard, autor del Quijote, Funes el memorioso, El Sur, El Aleph o Ulrica. Leer estos cuentos supone releer la historia de la humanidad y emprender una de las aventuras mÃ¡s enriquecedoras, bellas y emocionantes de todos los tiempos.', 'https://imagessl3.casadellibro.com/a/l/t0/13/9788426420213.jpg'),
(2, 'H.P. Lovecraft Anotado', 'H.P. Lovecraft', 'Akal', 'Castellano', 65, 'Pese a que la obra de Lovecraft fue ignorada por el pÃºblico y denostada por la crÃ­tica tras su muerte, tuvo tiempo despuÃ©s un reconocimiento sin precedentes, siendo reconocido un siglo despuÃ©s de su nacimiento como el autor que puso los cimientos de los relatos de horror y la ciencia ficciÃ³n estadounidense, convirtiÃ©ndose en una fuente de Â«influencia incalculable sobre sucesivas generaciones de escritores de relatos de miedo de ficciÃ³nÂ» En este volumen, Leslie S. Klinger da nueva vida a Lovecraft con claridad y visiÃ³n retrospectiva, y traza el progresivo reconocimiento de un escritor cuyo redescubrimiento y recuperaciÃ³n dentro del canon literario se puede comparar sÃ³lo a la de Poe o Melville. Aunando los documentados estudios existentes con sus propios puntos de vista, Klinger anota la sorprendente obra de Lovecraft y su kafkiana vida dotÃ¡ndola de contexto y desvelando muchos de los secretos que a menudo han motivado que el estilo de Lovecraft se definiese como crÃ­ptico.', 'https://imagessl7.casadellibro.com/a/l/t0/67/9788446043867.jpg'),
(3, 'HiperbÃ³rea', 'Clark Ashton Smith', 'Valdemar', 'Castellano', 24, 'Muchos de estos relatos se desarrollan en mundos perdidos, escenarios fantÃ¡sticos situados en un lejano futuro o en un remotÃ­simo pasado, en la tierra o en otros planetas (tambiÃ©n en la colecciÃ³n GÃ³tica Â«Zothique, el Ãºltimo continenteÂ»); son cuentos teÃ±idos de exotismo, ironÃ­a y crueldad, que rivalizan en su propio e inconfundible estilo con las odiseas bÃ¡rbaras de Howard y los horrores cÃ³smicos de Lovecraft. Este volumen reÃºne las diez historias dedicadas por Smith al mundo perdido de HiperbÃ³rea: una civilizaciÃ³n prehistÃ³rica, anterior a la Ãºltima glaciaciÃ³n, un universo crepuscular condenado a desaparecer bajo la nieve y el hielo.', 'https://imagessl2.casadellibro.com/a/l/t0/82/9788477027782.jpg'),
(4, 'Soy Leyenda', 'Richard Matheson', 'Minotauro', 'Castellano', 7.95, 'El Ãºltimo hombre sobre la Tierra no estÃ¡ solo.\r\nRobert Neville es el Ãºnico superviviente de una guerra bacteriolÃ³gica que ha asolado el planeta y convertido al resto de la humanidad en vampiros. Su vida se ha reducido a asesinar el mÃ¡ximo nÃºmero posible de estos seres sanguinarios durante el dÃ­a, y a soportar su asedio cada noche. Para ellos, el autÃ©ntico monstruo es este hombre que lucha por subsistir en un nuevo orden establecido.\r\nTodo un clÃ¡sico en su gÃ©nero, Ã©ste es un perturbador relato sobre la soledad y el aislamiento y una reflexiÃ³n sobre los binomios como normalidad y anormalidad, bien y mal, que se evidencian como una mera convenciÃ³n derivada del temor y el desconcierto ante lo diferente.', 'https://imagessl1.casadellibro.com/a/l/t0/61/9788445003961.jpg'),
(5, 'Dune (Saga Dune 1)', 'Frank Herbert', 'Debolsillo', 'Castellano', 10.95, 'Dune: una obra maestra unÃ¡nimemente reconocida como la mejor saga de ciencia ficciÃ³n de todos los tiempos.\r\n\r\nArrakis: un planeta desÃ©rtico donde el agua es el bien mÃ¡s preciado y donde llorar a los muertos es el sÃ­mbolo de mÃ¡xima prodigalidad.\r\n\r\nPaul Atreides: un adolescente marcado por un destino singular, dotado de extraÃ±os poderes y abocado a convertirse en dictador, mesÃ­as y mÃ¡rtir.\r\n\r\nLos Harkonnen: personificaciÃ³n de las intrigas que rodean el Imperio GalÃ¡ctico, buscan obtener el control sobre Arrakis para disponer de la melange, preciosa especia y uno de los bienes mÃ¡s codiciados del universo.\r\n\r\nLos Fremen: seres libres que han convertido el inhÃ³spito paraje de Dune en su hogar, y que se sienten orgullosos de su pasado y temerosos de su futuro.', 'https://imagessl4.casadellibro.com/a/l/t0/24/9788497596824.jpg'),
(6, 'El Hombre En El Castillo', 'Philip K. Dick', 'Minotauro', 'Castellano', 7.95, 'Los caracteres de El hombre en el castillo no son sÃ³lo productos de la imaginaciÃ³n sino tambiÃ©n manifestaciones de un sistema de fuerza en el que el I Ching obra como un nexo anÃ¡logo a un polo magnÃ©tico ... La mejor novela hasta la fecha del escritor de c-f mÃ¡s consistentemente brillante. Ante todo por la verosimilitud con que Dick ha elaborado una ingeniosa estructura bÃ¡sica (unos Estados Unidos derrotados en la segunda guerra mundial y que han sido divididos en tres partes: las costas del AtlÃ¡ntico y del Pacifico respectivamente ocupadas por alemanes y japoneses, y una zona tapÃ³n entre dos esferas de influencia); luego por la absoluta originalidad de la presunciÃ³n bÃ¡sica--los alemanes ganaron la segunda guerra--aunque el Libro de los Cambios informa sin embargo que esta amarga derrota no ha ocurrido en el mundo real... \" JOHN BRUNNER', 'https://imagessl4.casadellibro.com/a/l/t0/44/9788445001844.jpg'),
(7, 'Fahrenheit 451', 'Ray Bradbury', 'Debolsillo', 'Castellano', 9.95, 'Fahrenheit 451: la temperatura a la que el papel se enciende y arde. Guy Montag es un bombero y el trabajo de un bombero es quemar libros, que estÃ¡n prohibidos porque son causa de discordia y sufrimiento.El Sabueso MecÃ¡nico del Departamento de Incendios, armado con una letal inyecciÃ³n hipodÃ©rmica, escoltado por helicÃ³pteros, estÃ¡ preparado para rastrear a los disidentes que aÃºn conservan y leen libros. Como 1984, de George Orwell, como Un mundo feliz, de Aldous Huxley, Fahrenheit 451 describe una civilizaciÃ³n occidental esclavizada por los medios, los tranquilizantes y el conformismo.La visiÃ³n de Bradbury es asombrosamente profÃ©tica: pantallas de televisiÃ³n que ocupan paredes y exhiben folletines interactivos; avenidas donde los coches corren a 150 kilÃ³metros por hora persiguiendo a peatones; una poblaciÃ³n que no escucha otra cosa que una insÃ­pida corriente de mÃºsica y noticias transmitidas por unos diminutos auriculares insertados en las orejas.Â«Fahrenheit 451 es el mÃ', 'https://imagessl8.casadellibro.com/a/l/t0/78/9788490321478.jpg'),
(8, 'Â¿SueÃ±an los androides con ovejas elÃ©ctricas?', 'Philip K. Dick', 'Minotauro', 'Castellano', 17.96, 'Nueva ediciÃ³n de esta obra clÃ¡sica de la ciencia ficciÃ³n en la colecciÃ³n Esenciales Minotauro. La fuente de inspiraciÃ³n para la pelÃ­cula de culto Blade Runner.\r\nEn el aÃ±o 2021 la guerra mundial ha exterminado a millones de personas. Los supervivientes codician cualquier criatura viva, y aquellos que no pueden permitirse pagar por ellas se ven obligados a adquirir rÃ©plicas increÃ­blemente realistas.\r\n\r\nLas empresas fabrican incluso seres humanos. Rick Deckard es un cazarrecompensas cuyo trabajo es encontrar androides rebeldes y retirarlos, pero la tarea no serÃ¡ tan sencilla cuando tenga que enfrentarse a los nuevos modelos Nexus-6, prÃ¡cticamente indistinguibles de los seres humanos.  ', 'https://imagessl7.casadellibro.com/a/l/t0/57/9788445006757.jpg'),
(9, 'DrÃ¡cula (ClÃ¡sicos Ilustrados)', 'Bram Stoker', 'Alma Europa', 'Castellano', 15.95, '', 'https://imagessl6.casadellibro.com/a/l/t0/36/9788415618836.jpg'),
(10, 'El Rey de Amarillo', 'Robert Chambers', 'Valdemar', 'Castellano', 24, 'Â«El Rey de Amarillo. Relatos macabros y terrorÃ­ficosÂ» invoca un mundo de caos y perdiciÃ³n, fascinante y repugnante al tiempo, que nos recuerda algunas obras de Meyrink e incluso del propio Kafka.', 'https://imagessl9.casadellibro.com/a/l/t0/19/9788477027119.jpg'),
(11, 'Cementerio de animales', 'Stephen King', 'Debolsillo', 'Castellano', 9.95, 'Church estaba allÃ­ otra vez, como Louis Creed temÃ­a y deseaba. Porque su hijita Ellie le habÃ­a encomendado que cuidara del gato, y Church habÃ­a muerto atropellado. Louis lo habÃ­a comprobado: el gato estaba muerto, incluso lo habÃ­a enterrado mÃ¡s allÃ¡ del cementerio de animales. Sin embargo, Church habÃ­a regresado, y sus ojos eran mÃ¡s crueles y perversos que antes. Pero volvÃ­a a estar allÃ­ y Ellie no lo lamentarÃ­a. Louis Creed sÃ­ lo lamentarÃ­a. Porque mÃ¡s allÃ¡ del cementerio de animales, mÃ¡s allÃ¡ de la valla de troncos que nadie se atrevÃ­a a trasponer, mÃ¡s allÃ¡ de los cuarenta y cinco escalones, el maligno poder del antiguo cementerio indio le reclamaba con macabra avidez... ï»¿', 'https://imagessl4.casadellibro.com/a/l/t0/94/9788497930994.jpg'),
(12, 'El Exorcista', 'William Peter Blatty', 'B de Bolsillo', 'Castellano', 8.95, 'Basada en un hecho real, ocurrido en la dÃ©cada de los cuarenta del siglo pasado, la protagonista es una niÃ±a de apenas once aÃ±os que sufre terribles transformaciones, sobre todo en su comportamiento. Ni mÃ©dicos ni cientÃ­ficos ni psicÃ³logos son capaces de hallar la causa de tal estado y paulatinamente todo se va decantando hacia la hipÃ³tesis de que la niÃ±a estÃ¡ poseÃ­da por el demonio.\r\n\r\nPublicada en 1971,El exorcistaes una de las novelas mÃ¡s sobrecogedoras y terrorÃ­ficas jamÃ¡s escritas, y ha dado origen a una saga cinematogrÃ¡fica y una seria de televisiÃ³n de culto.', 'https://imagessl3.casadellibro.com/a/l/t0/43/9788490707043.jpg'),
(13, 'Libros de Sangre (VolÃºmenes I, II y III)', 'Clive Barker', 'Valdemar', 'Castellano', 32, 'Este volumen reÃºne las mejores historias cortas de Clive Barker, fruto de dieciocho meses de trabajo nocturno, ya que por el dÃ­a Barker se dedicaba a escribir obras de teatro. Relatos como El Tren de Carne de Medianoche, una historia enraizada en las pelÃ­culas de terror mÃ¡s explÃ­citas, El blues de la sangre de cerdo, mezcla de Â«El seÃ±or de las moscasÂ» y Â«La naranja mecÃ¡nicaÂ», Terror, de un sadismo entre lo explÃ­cito y el voyeurismo, o Restos humanos, obra maestra de la ficciÃ³n macabra moderna.', 'https://imagessl7.casadellibro.com/a/l/t0/07/9788477028307.jpg'),
(14, 'Libros de Sangre II (VolÃºmenes IV, V y VI)', 'Clive Barker', 'Valdemar', 'Castellano', 32, 'Este segundo y definitivo tomo de Libros de sangre (1984-1985), obra maestra de Clive Barker, reÃºne los volÃºmenes IV, V y VI. Entre los relatos incluidos en este tomo, podrÃ­amos destacar La Madonna, en el que un universo lÃ­quido, viscoso y mercurial palpita bajo el suelo londinense, CrepÃºsculo en las torres, que nos ofrece un nuevo antihÃ©roe, un mesÃ­as salvaje que recuerda al protagonista de su novela Â«CabalÂ» (1988), Â“La Edad del DeseoÂ”, cuyo trÃ¡gico protagonista ha sido vÃ­ctima de un experimento digno del primer Cronenberg, o El cuerpo polÃ­tico, en el que el lector asistirÃ¡ incrÃ©dulo a la truculenta rebeliÃ³n de los miembros de un cuerpo humano decididos a independizarse de este de forma sangrienta.', 'https://imagessl6.casadellibro.com/a/l/t0/66/9788477028666.jpg'),
(15, 'IT', 'Stephen King', 'Debolsillo', 'Castellano', 12.95, 'Tras lustros de tranquilidad y lejania una antigua promesa infantil les hace volver al lugar en el que vivieron su infancia y juventud como una terrible pesadilla. Regresan a Derry para enfrentarse con su pasado y enterrar definitivamente la amenaza que los amargÃ³ durante su niÃ±ez. Saben que pueden morir, pero son conscientes de que no conocerÃ¡n la paz hasta que aquella cosa sea destruida para siempre. It es una de las novelas mÃ¡s ambiciosas de Stephen King, donde ha logrado perfeccionar de un modo muy personal las claves del gÃ©nero de terror.', 'https://imagessl3.casadellibro.com/a/l/t0/93/9788497593793.jpg'),
(16, 'El Resplandor', 'Stephen King', 'Debolsillo', 'Castellano', 10.95, 'Â¿QuÃ© ha sido de Danny Torrance? DescÃºbrelo al final de este volumen, que incluye el inicio de Doctor SueÃ±o, la continuaciÃ³n de El resplandor.REDRUM. Esa es la palabra que Danny habÃ­a visto en el espejo. Y, aunque no sabÃ­a leer, entendiÃ³ que era un mensaje de horror. TenÃ­a cinco aÃ±os, y a esa edad pocos niÃ±os son conscientes de que los espejos invierten las imÃ¡genes, y menos aÃºn diferencian entre realidad y fantasÃ­a. Pero Danny tenÃ­a pruebas de que sus fantasÃ­as relacionadas con el resplandor del espejo acabarÃ­an cumpliÃ©ndose: REDRUM... MURDER, asesinato.Su madre estaba pensando en el divorcio, y su padre, obsesionado con algo muy malo, tan malo como la muerte y el suicidio, necesitaba aceptar la propuesta de cuidar de aquel hotel de lujo, de mÃ¡s de cien habitaciones aislado por la nieve, durante seis meses. Hasta el deshielo iban a estar solos. Â¿Solos?...Â«TerrorÃ­fica... ofrece horrores a un ritmo intenso e infatigable.Â»The New York Times', 'https://imagessl9.casadellibro.com/a/l/t0/29/9788490328729.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `rol` enum('Gestor','Cliente','Administrador') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `email`, `password`, `rol`) VALUES
(1, 'Oscar', 'oscar@gmail.com', '8a3cac88b6be85cabe96ba62deee5bf742501f7e209080290653f856fc482517ee996e8f4eeeb78cd8c8ea1a933f7c6bc47f101f543dfbf18b7bd2f7f3361a82', 'Cliente'),
(2, 'Sergio', 'sergio@gmail.com', '8a3cac88b6be85cabe96ba62deee5bf742501f7e209080290653f856fc482517ee996e8f4eeeb78cd8c8ea1a933f7c6bc47f101f543dfbf18b7bd2f7f3361a82', 'Gestor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idarticulo`,`idusuario`,`fecha`),
  ADD KEY `idusuario` (`idusuario`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`idarticulo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `idarticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compras_ibfk_1` FOREIGN KEY (`idarticulo`) REFERENCES `libros` (`idarticulo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `compras_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
