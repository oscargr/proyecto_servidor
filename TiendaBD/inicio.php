<?php

session_start();
$_SESSION['bd']="tienda";
$_SESSION['server']="localhost";
$_SESSION['usu1']="ClienteTienda";
$_SESSION['pass1']="ClienteTienda";
$_SESSION['usu2']="AdminTienda";
$_SESSION['pass2']="AdminTienda";
$_SESSION['usu3']="GestorTienda";
$_SESSION['pass3']="GestorTienda";

?>

<!DOCTYPE html>
<html>
<head>
	<title>Inicio - Tienda Online</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="icon" href="icono.ico" type="image/ico" sizes="16x16">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Lekton|Special+Elite&display=swap');
	</style>
</head>
<body>
	<div class="cabecera">
		<div class="logo">
			<img src="logo.png" width="250">
		</div>
		<div class="nombre">
			<h1>Miskatonic Bookshop</h1>
		</div>
		<div class="opciones">
			<form action="" method="POST">
				<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Cerrar sesión" type="submit" name="logout"><img src="logout.png" width="64"></button>
			</form>
		</div>
	</div>
	<div class="bienvenida">
		<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?></h2>
	</div>
	<?php

	if ($_SESSION['rol']=="Cliente") {

	require("inicioCliente.php");

	}

	if ($_SESSION['rol']=="Administrador") {
	
	require("inicioAdmin.php");

	}

	if ($_SESSION['rol']=="Gestor") {

	require("inicioGestor.php");		

	}

	?>
	<div class="piepagina">
		<div>
			<table>
				<tr>
					<th colspan="3">
						<h2>Contacto</h2>
					</th>
				</tr>
				<tr>
					<td>
						<img src="fb.png" width="64" alt="Logo de contacto por Facebook">
					</td>
					<td>
						<img src="ig.png" width="64" alt="Logo de contacto por Instagram">
					</td>
					<td>
						<img src="twitter.png" width="64" alt="Logo de contacto por Twitter">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						2020 &copy; Todos los derechos reservados
					</td>
				</tr>
			</table>
		</div>
	</div>
	<script>
		var modal = document.getElementById('modalB');

		var alto = screen.availHeight;

		document.getElementById('gestorbd').style.height=alto+"px";

		/*window.onclick = function(event) {
		  if (event.target == modal) {
		    modal.style.display = "none";
		  }
		}*/
	</script>
</body>
</html>