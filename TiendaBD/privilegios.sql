# Privilegios para `AccesoTienda`@`localhost`

GRANT USAGE ON *.* TO 'AccesoTienda'@'localhost' IDENTIFIED BY PASSWORD '*DB62620FBC1BB27F6B2EBE188C7AFE2D8D6C0BFC';

GRANT SELECT ON `tienda`.`usuarios` TO 'AccesoTienda'@'localhost';


# Privilegios para `AdminTienda`@`localhost`

GRANT USAGE ON *.* TO 'AdminTienda'@'localhost' IDENTIFIED BY PASSWORD '*421D702F0CEFE7EB9BCCD5AC5BC3599640DA58C4';

GRANT ALL PRIVILEGES ON `tienda`.* TO 'AdminTienda'@'localhost' WITH GRANT OPTION;


# Privilegios para `ClienteTienda`@`localhost`

GRANT USAGE ON *.* TO 'ClienteTienda'@'localhost' IDENTIFIED BY PASSWORD '*E00612DF53B7620F804132A4B33FA421ED31439C';

GRANT SELECT ON `tienda`.`libros` TO 'ClienteTienda'@'localhost';

GRANT SELECT, INSERT ON `tienda`.`compras` TO 'ClienteTienda'@'localhost';


# Privilegios para `GestorTienda`@`localhost`

GRANT USAGE ON *.* TO 'GestorTienda'@'localhost' IDENTIFIED BY PASSWORD '*821D817E64D3AA858E4EEF50EBDB6AAB2980D820';

GRANT SELECT, INSERT, UPDATE ON `tienda`.`libros` TO 'GestorTienda'@'localhost';

GRANT SELECT ON `tienda`.`usuarios` TO 'GestorTienda'@'localhost';

GRANT SELECT ON `tienda`.`compras` TO 'GestorTienda'@'localhost';


# Privilegios para `RegistroTienda`@`localhost`

GRANT USAGE ON *.* TO 'RegistroTienda'@'localhost' IDENTIFIED BY PASSWORD '*D3588EED4FF721FAD628CD87D61EF1724985F4AD';

GRANT INSERT ON `tienda`.`usuarios` TO 'RegistroTienda'@'localhost';