<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Historial de compras <?php echo $_SESSION['usuario']; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="icon" href="icono.ico" type="image/ico" sizes="16x16">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Lekton|Special+Elite&display=swap');
	</style>
</head>
<body>
	<div class="cabecera">
		<div class="logo">
			<img src="logo.png" width="250">
		</div>
		<div class="nombre">
			<h1>Miskatonic Bookshop</h1>
		</div>
		<div class="opciones">
			<form action="" method="POST">
				<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Volver a la página anterior" type="submit" name="back"><img src="back.png" width="64"></button>
				<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Cerrar sesión" type="submit" name="logout"><img src="logout.png" width="64"></button>
			</form>
		</div>
	</div>
	<div class="bienvenida">
		<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?></h2>
	</div>
	<div class="main">
		<div class="busqueda">
			<form action="" method="POST" onsubmit="return validar()">
				<h3>Ver compras</h3>
				<p>Desde <input type="date" name="fechain" id="fechain"></p>
				<p>Hasta <input type="date" name="fechafin" id="fechafin" onblur="validarfecha()"><span id="avisofecha"></span></p>
				<p>
					<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Comprobar historial de compras" type="submit" name="search">
						<img src="search.png" alt="Botón para comprobar el historial de compras entre las fechas introducidas" width="32">
					</button>
				</p>
				<p>Si quiere ver todas sus compras haga click en la lupa sin introducir ninguna fecha</p>
			</form>
		</div>

	<div id="modalF" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalF').style.display='none'" class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p>La segunda fecha ha de ser posterior a la primera</p>
    		</div>
    	</div>
    </div>

    <div id="modalE" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalE').style.display='none'" class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p>Datos erróneos, vuelva a introducirlos</p>
    		</div>
    	</div>
    </div>

	<?php

	if (isset($_POST['search'])) {
		
		$fechain=$_POST['fechain'];
		$fechafin=$_POST['fechafin'];
		$idusu=$_SESSION['id'];
		if ($fechain!="" && $fechafin!="") {
			$fechain=$fechain." 00:00:00";
			$fechafin=$fechafin." 23:59:59";
		}

		$conexion=mysqli_connect($_SESSION['server'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['bd']);
		if (mysqli_connect_errno()) {
			printf("Conexión fallida %s\n", mysqli_connect_error());
			exit();
		}

	if ($fechain!="" || $fechafin!="") {

	?>
	
	<div class="compras">
		<table border="1">
			<tr>
				<th>Fecha</th>
				<th>Título</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>Total</th>
			</tr>

	<?php	

		$sql="SELECT compras.fecha,libros.titulo,compras.cantidad,compras.precio,compras.cantidad*compras.precio FROM compras, libros WHERE compras.idusuario=$idusu AND libros.idarticulo=compras.idarticulo AND compras.fecha BETWEEN '$fechain' AND '$fechafin' ORDER BY compras.fecha ASC;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {

	?>
	
			<tr>
				<td><?php echo $registro[0]; ?></td>
				<td><?php echo $registro[1]; ?></td>
				<td><?php echo $registro[2]; ?></td>
				<td><?php echo $registro[3]; ?>€</td>
				<td><?php echo round($registro[4], 2); ?>€</td>
			</tr>			

	<?php			

			}
		}
		else {

	?>
	
			<tr>
				<td colspan="5">
					<p>No se ha realizado ninguna compra entre esas fechas</p>
				</td>
			</tr>

	<?php		

		}

	?>
	
		</table>
	</div>

	<?php

	}

	else {

	?>
	
	<div class="compras">
		<table border="1">
			<tr>
				<th>Fecha</th>
				<th>Título</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>Total</th>
			</tr>

	<?php

	$sql="SELECT compras.fecha,libros.titulo,compras.cantidad,compras.precio,compras.cantidad*compras.precio FROM compras, libros WHERE compras.idusuario=$idusu AND libros.idarticulo=compras.idarticulo ORDER BY compras.fecha ASC;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {

	?>
	
			<tr>
				<td><?php echo $registro[0]; ?></td>
				<td><?php echo $registro[1]; ?></td>
				<td><?php echo $registro[2]; ?></td>
				<td><?php echo $registro[3]; ?>€</td>
				<td><?php echo round($registro[4], 2); ?>€</td>
			</tr>

	<?php

			}
		}

		else {

	?>

			<tr>
				<td colspan="5">
					<p>No se ha realizado ninguna compra todavía</p>
				</td>
			</tr>

	<?php
	
		}

	?>		
	
		</table>
	</div>

	<?php			

	}

		mysqli_close($conexion);
		
	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	?>

	</div>
	<div class="piepagina">
		<div>
			<table>
				<tr>
					<th colspan="3">
						<h2>Contacto</h2>
					</th>
				</tr>
				<tr>
					<td>
						<img src="fb.png" width="64" alt="Logo de contacto por Facebook">
					</td>
					<td>
						<img src="ig.png" width="64" alt="Logo de contacto por Instagram">
					</td>
					<td>
						<img src="twitter.png" width="64" alt="Logo de contacto por Twitter">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						2020 &copy; Todos los derechos reservados
					</td>
				</tr>
			</table>
		</div>
	</div>

	<script>
		function validar() {
			if (validarfecha()) {
				return true;
			}
			else {
				document.getElementById('modalF').style.display="none";
				document.getElementById('modalE').style.display="block";
				return false;
			}
		}

		function validarfecha() {
			var fechain = document.getElementById('fechain');
			var fechafin = document.getElementById('fechafin');
			var aviso = document.getElementById('avisofecha');

			if (fechafin.value<fechain.value) {
				fechafin.style.outline="2px outset red";
				aviso.innerHTML=" &cross;";
				document.getElementById('modalF').style.display="block";
				return false;
			}
			else {
				fechafin.style.outline="2px outset green";
				aviso.innerHTML=" &check;";
				return true;
			}
		}
	</script>
</body>
</html>