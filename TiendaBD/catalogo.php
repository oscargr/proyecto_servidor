<?php
ob_start();
session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Catálogo</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="icon" href="icono.ico" type="image/ico" sizes="16x16">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Lekton|Special+Elite&display=swap');
	</style>
</head>
<body>
	<div class="cabecera">
		<div class="logo">
			<img src="logo.png" width="250">
		</div>
		<div class="nombre">
			<h1>Miskatonic Bookshop</h1>
		</div>
		<div class="opciones">
			<form action="" method="POST">
				<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Volver a la página anterior" type="submit" name="back"><img src="back.png" width="64"></button>
				<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Cerrar sesión" type="submit" name="logout"><img src="logout.png" width="64"></button>
			</form>
		</div>
	</div>
	<div class="bienvenida">
		<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?></h2>
	</div>
	<div class="main">
		<div class="libros">
			<form action="" method="POST">
			<?php 

			$conexion=mysqli_connect($_SESSION['server'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['bd']);
			if (mysqli_connect_errno()) {
			    printf("Conexión fallida %s\n", mysqli_connect_error());
			    exit();
			}

			$sql="SELECT * FROM libros";
			$result = mysqli_query ($conexion, $sql);
			$filas=mysqli_num_rows($result);
			if ($filas>0) {
				while ($registro = mysqli_fetch_row($result)) {
					if (!isset($_SESSION['carro'][$registro[0]])) {
						$_SESSION['carro'][$registro[0]]=0;
					}
					$_SESSION['precios'][$registro[0]]=$registro[5];
					$_SESSION['titulos'][$registro[0]]=$registro[1];
					$_SESSION['resumenes'][$registro[0]]=$registro[6];

			?>

			<div class="libro">
				<table>
					<tr>
						<td rowspan="6">
							<img class="portadalibro" alt="Portada libro" height="150" src=<?php echo $registro[7]; ?>>
						</td>
						<td>
							<?php echo $registro[1]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $registro[2]; ?>
						</td>
					</tr>
					<tr>
						<td>
							Ed. <?php echo $registro[3]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $registro[4]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $registro[5]; ?>€
						</td>
					</tr>
					<tr>
						<td>
							<button style="cursor: pointer;" type="submit" name="resumen" value=<?php echo $registro[0]; ?>>Leer resumen</button>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Añadir libro a la cesta" type="submit" name="add" value=<?php echo $registro[0]; ?>>
								<img src="add.png" width="64" alt="Botón para añadir el libro al carro">
							</button>
						</td>
					</tr>
				</table>
			</div>

			<?php

				}
			}

			?>
			</form>
		</div>

		<div class="carro">
			<form action="" method="POST">
				<?php

				$total=0;
				foreach ($_SESSION['carro'] as $id => $cantidad) {
					if ($cantidad>0) {
						$precio=($cantidad*$_SESSION['precios'][$id]);
						$total+=$precio;
					}
				}
				$_SESSION['total']=$total;

				if ($_SESSION['total']!=0) {

				?>

				<style>
					.libros {
						flex: 75%;
					}

					.carro {
						flex: 15%;
					}

					.carro form {
						background-color: rgba(0,0,0,0.5);
						border-radius: 5% 0% 0% 5%;
						margin-right: 50px;
						padding: 20px 0px;
					}


					@media screen and (max-width: 700px) {
						.carro form {
							margin-right: 0px;
							margin-top: 25px;
						}
					}
				</style>
				
				<h2>Cesta de la compra</h2>

				<?php

				foreach ($_SESSION['carro'] as $id => $cantidad) {
					if ($cantidad>0) {
				
				?>

				<p><?php 

							if ($cantidad==1) {
								echo "<p>".$_SESSION['titulos'][$id].": ".$cantidad." Unidad</p>";
							}
							else {
								echo "<p>".$_SESSION['titulos'][$id].": ".$cantidad." Unidades</p>";
							}

				?></p><p><button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Quitar libro de la cesta" type="submit" name="del" value=<?php echo $id; ?>>
					<img class="delete" src="del.png" width="32" alt="Botón para quitar el libro del carro">
				</button></p>
				<hr style="width: 80%;">

				<?php

					}
				}

				?>
				<p style="font-size: 24px;"><strong>Total: <?php echo $_SESSION['total']; ?>€</strong></p>
				<p><button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Finalizar compra" type="submit" name="fincompra">
					<img class="buy" src="buy.png" width="64" alt="Botón para proceder al pago">
				</button></p>
			</form>
				<?php

				}

				?>
		</div>
	</div>
	<div class="piepagina">
		<div>
			<table>
				<tr>
					<th colspan="3">
						<h2>Contacto</h2>
					</th>
				</tr>
				<tr>
					<td>
						<img src="fb.png" width="64" alt="Logo de contacto por Facebook">
					</td>
					<td>
						<img src="ig.png" width="64" alt="Logo de contacto por Instagram">
					</td>
					<td>
						<img src="twitter.png" width="64" alt="Logo de contacto por Twitter">
					</td>
				</tr>
				<tr>
					<td colspan="3">
						2020 &copy; Todos los derechos reservados
					</td>
				</tr>
			</table>
		</div>
	</div>

	<?php

	if (isset($_POST['resumen'])) {
		$idlibro = $_POST['resumen'];

	?>

	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'" class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<h2>Resumen <?php echo $_SESSION['titulos'][$idlibro]; ?></h2>
    			<p><?php
    			if (strlen($_SESSION['resumenes'][$idlibro])>0) {
    				echo $_SESSION['resumenes'][$idlibro];
    			}
    			else {
    				echo "No podemos ofrecerle este resumen";
    			}
    			?></p>
    		</div>
    	</div>
    </div>

	<?php

	}

	if (isset($_POST['fincompra'])) {
		
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<h2>Cesta de la compra</h2>
    			<ul>
    			<form action="" method="POST">
	    			<?php

	    			foreach ($_SESSION['carro'] as $id => $cantidad) {
						if ($cantidad>0) {
							if ($cantidad==1) {
								echo "<li>".$_SESSION['titulos'][$id].": ".$cantidad." Unidad</li>";
							}
							else {
								echo "<li>".$_SESSION['titulos'][$id].": ".$cantidad." Unidades</li>";
							}
						}
					}

	    			?>
	    			<p style="font-size: 24px;"><strong>Total: <?php echo $_SESSION['total']; ?>€</strong></p>
	    			<p><button style="border: none; outline: none; background-color: transparent; cursor: pointer;" title="Confirmar pedido" type="submit" name="confirmar">
	    				<img src="entrega.png" width="64" alt="Botón para confirmar pedido para entregar"><br><span style="color: white;">Confirmar pedido</span>
	    			</button>
	    			</p>	
    			</form>
    			</ul>
    		</div>
    	</div>
    </div>

	<?php

	}

	if (isset($_POST['confirmar'])) {
		$idusu=$_SESSION['id'];
		$fecha=date("Y-m-d H:i:s");
		foreach ($_SESSION['carro'] as $idart => $cantidad) {
			if ($cantidad>0) {
				$precio=$_SESSION['precios'][$idart];
				$sql="INSERT INTO compras (idarticulo,idusuario,fecha,cantidad,precio) VALUES ($idart,$idusu,'$fecha',$cantidad,$precio);";
				if (mysqli_query($conexion, $sql)) {
					$mensajeregistro=$_SESSION['usuario'].", se ha realizado su compra con éxito";
				}
				else {
					echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
				}
			}
		}
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha" style="padding: 0px; width: 42px; height: 37px;"><form action="" method="POST"><button type="submit" name="reini" style="background-color: rgba(0,0,0,0); outline: none; border: none; cursor: pointer; width: 100%; height: 100%; font-size: 18px; padding-top: 8px;">&times;</button></form></span>
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php

	foreach ($_SESSION['carro'] as $idart => $cantidad) {
		$_SESSION['carro'][$idart]=0;
	}

	if (isset($_POST['reini'])) {
		header("Location:catalogo.php");
	}

	}

	if (isset($_POST['add'])) {
		$prod = $_POST['add'];
		$_SESSION['carro'][$prod]++;
		header("Location:catalogo.php");
	}

	if (isset($_POST['del'])) {
		$prod = $_POST['del'];
		if ($_SESSION['carro'][$prod]>0) {
			$_SESSION['carro'][$prod]--;
			header("Location:catalogo.php");
		}
	}

	if (isset($_POST['historial'])){
		header("Location:historial.php");
	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	mysqli_close($conexion);
	ob_end_flush();

	?>
</body>
</html>