<?php

ob_start();

?>	
	<div class="main">
		<div class="menuGestion">
			<form action="" method="POST">
				<input type="submit" name="insArt" value="Insertar artículo">
				<input type="submit" name="verArt" value="Ver artículos">
			</form>
		</div>
	</div>
	<p style="text-align: center;">Nuesto cátalogo disponible actualmente, haga scroll en los libros para moverse</p>
	<div class="librosDispo" id="librosDispo" >
<?php

			$conexion=mysqli_connect('localhost', 'ClienteTienda', 'ClienteTienda', 'tienda');
			if (mysqli_connect_errno()) {
			    printf("Conexión fallida %s\n", mysqli_connect_error());
			    exit();
			}

			$sql="SELECT * FROM libros";
			$result = mysqli_query ($conexion, $sql);
			$filas=mysqli_num_rows($result);
			if ($filas>0) {
				while ($registro = mysqli_fetch_row($result)) {
					$_SESSION['imagenes'][$registro[0]]=$registro[7];
					$_SESSION['precios'][$registro[0]]=$registro[5];
					$_SESSION['titulos'][$registro[0]]=$registro[1];
				}
			}

foreach ($_SESSION['imagenes'] as $idart => $src) {
	
?>

	<div class="portada">
		<img height="250" src=<?php echo $src; ?>>
		<div class="titulo">
			<p><?php echo $_SESSION['titulos'][$idart]; ?><br><?php echo $_SESSION['precios'][$idart] ?>€</p>
		</div>
	</div>

<?php


}

?>

</div>

<script>
	(function() {
	    function scrollHorizontally(e) {
	        e = window.event || e;
	        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
	        document.getElementById('librosDispo').scrollLeft -= (delta*40); // Multiplied by 40
	        e.preventDefault();
	    }
	    if (document.getElementById('librosDispo').addEventListener) {
	        // IE9, Chrome, Safari, Opera
	        document.getElementById('librosDispo').addEventListener("mousewheel", scrollHorizontally, false);
	        // Firefox
	        document.getElementById('librosDispo').addEventListener("DOMMouseScroll", scrollHorizontally, false);
	    } else {
	        // IE 6/7/8
	        document.getElementById('librosDispo').attachEvent("onmousewheel", scrollHorizontally);
	    }
	})();
</script>

	<?php

	if (isset($_POST['insArt'])) {

	?>

	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<table style="margin: 0px auto;">
    				<tr>
    					<th colspan="2">
    						<h2>Añadir libro al catálogo</h2>
    					</th>
    				</tr>
    			<form action="" method="POST">
    				<tr>
	    				<td>Título: </td><td><input type="text" name="titulo" required="required" maxlength="300" placeholder="*Campo obligatorio"></td>
	    			</tr>
	    			<tr>
	    				<td>Autor: </td><td><input type="text" name="autor" required="required" maxlength="50" placeholder="*Campo obligatorio"></td>
	    			</tr>
	    			<tr>
	    				<td>Editorial: </td><td><input type="text" name="editorial" required="required" maxlength="300" placeholder="*Campo obligatorio"></td>
	    			</tr>
	    			<tr>
	    				<td>Idioma: </td><td><input type="text" name="idioma" required="required" maxlength="150" placeholder="*Campo obligatorio"></td>
	    			</tr>
	    			<tr>
	    				<td>Precio: </td><td><input type="number" name="precio" required="required" placeholder="*Campo obligatorio" step="0.01"></td>
	    			</tr>
	    			<tr>
	    				<td colspan="2">Resumen: </td>
	    			</tr>
	    			<tr>
	    				<td colspan="2">
	    					<textarea name="resumen"></textarea>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td colspan="2">Portada: </td>
	    			</tr>
	    			<tr>
	    				<td colspan="2">
	    					<input type="url" name="portada" required="required" maxlength="1000" placeholder="URL de la portada" placeholder="*Campo obligatorio" style="width: 100%;">
	    				</td>
	    			</tr>
	    			<tr>
	    				<td colspan="2"><input type="submit" name="insertar" value="Insertar artículo"></td>
	    			</tr>
    			</form>
    			</table>
    		</div>
    	</div>
    </div>

	<?php	

	}

		if (isset($_POST['insertar'])) {
			$titulo=$_POST['titulo'];
			$autor=$_POST['autor'];
			$editorial=$_POST['editorial'];
			$idioma=$_POST['idioma'];
			$precio=$_POST['precio'];
			$resumen=$_POST['resumen'];
			$portada=$_POST['portada'];

			$conexion=mysqli_connect($_SESSION['server'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['bd']);
			if (mysqli_connect_errno()) {
				printf("Conexión fallida %s\n", mysqli_connect_error());
				exit();
			}

			$sql="INSERT INTO libros (idarticulo,titulo,autor,editorial,idioma,precio,resumen,imagen) VALUES (NULL,'$titulo','$autor','$editorial','$idioma',$precio,'$resumen','$portada');";
			if (mysqli_query($conexion, $sql)) {
				$mensajeregistro=$_SESSION['usuario'].", ha añadido el libro con éxito";

	?>

	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'" class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php

			}
			else {
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
			}

			mysqli_close($conexion);
		}

	if (isset($_POST['verArt'])) {
		$conexion=mysqli_connect($_SESSION['server'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['bd']);
		if (mysqli_connect_errno()) {
			printf("Conexión fallida %s\n", mysqli_connect_error());
			exit();
		}

	?>
	<div class="main">
	<div class="bdlibros">
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Título</th>
			<th>Autor</th>
			<th>Editorial</th>
			<th>Idioma</th>
			<th>Precio</th>
		</tr>

	<?php	

		$sql="SELECT idarticulo, titulo, autor, editorial, idioma, precio FROM libros;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {
	
	?>
	
		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><?php echo $registro[5]; ?>€</td>
		</tr>

	<?php

			}
		}

	?>

	</table>
	</div>
	</div>

	<?php

	mysqli_close($conexion);

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	ob_end_flush();