<?php

session_start();
$_SESSION['servidor']='localhost';
$_SESSION['basedatos']='consultas';
$_SESSION['usu1']='admin';
$_SESSION['pass1']='admin';
$_SESSION['usu2']='medico';
$_SESSION['pass2']='medico';
$_SESSION['usu3']='asistente';
$_SESSION['pass3']='asistente';
$_SESSION['usu4']='paciente';
$_SESSION['pass4']='paciente';

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Inicio <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	<?php

	if (isset($_SESSION['nif']) && isset($_SESSION['rol'])) {

		if ($_SESSION['rol']=="Administrador") {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><img src="paciente.png" width="64" alt="Icono paciente"><br/><button type="submit" name="altap">Alta Paciente</button></div>
			<div><img src="doctor.png" width="64" alt="Icono doctor"><img src="doctora.png" width="64" alt="Icono doctora"><br/><button type="submit" name="altam">Alta Médico</button></div>
			<div><img src="logout.png" width="64" alt="Icono cerrar sesión"><br/><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>

	<?php

			if (isset($_POST['altap'])) {

				header("Location:altap.php");

			}

			if (isset($_POST['altam'])) {

				header("Location:altam.php");

			}

			if (isset($_POST['logout'])) {

				session_destroy();
		 
				header("Location:index.php");
			}		

		}

		if ($_SESSION['rol']=='Medico') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT medNombres,medApellidos FROM medicos WHERE dniMed='".$_SESSION['nif']."';";
			$result = mysqli_query ($conexion, $sql);

			while ($registro = mysqli_fetch_row($result)) {
			   $_SESSION['nom']=$registro[0];
			   $_SESSION['apell']=$registro[1];
			}

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><img src="atendida.png" width="64" alt="Icono citas atendidas"><br/><button type="submit" name="citasa">Ver Citas Atendidas</button></div>
			<div><img src="pendiente.png" width="64" alt="Icono citas pendientes"><br/><button type="submit" name="citasp">Ver Citas Pendientes</button></div>
			<div><img src="carpeta.png" width="64" alt="Icono lista pacientes"><br/><button type="submit" name="vpac">Ver Pacientes</button></div>
			<div><img src="logout.png" width="64" alt="Icono cerrar sesión"><br/><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>	

	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['citasp'])) {

				header("Location:citasPendientes.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:index.php");
			}	

		}

		if ($_SESSION['rol']=='Asistente') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><img src="atendida.png" width="64" alt="Icono citas atendidas"><br/><button type="submit" name="citasa">Ver Citas Atendidas</button></div>
			<div><img src="pendiente.png" width="64" alt="Icono nueva cita"><br/><button type="submit" name="nuevacita">Nueva Cita</button></div>
			<div><img src="paciente.png" width="64" alt="Icono paciente"><br/><button type="submit" name="altap">Alta Paciente</button></div>
			<div><img src="carpeta.png" width="64" alt="Icono lista pacientes"><br/><button type="submit" name="vpac">Ver Pacientes</button></div>
			<div><img src="logout.png" width="64" alt="Icono cerrar sesión"><br/><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>	

	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['nuevacita'])) {

				header("Location:nuevaCita.php");

			}

			if (isset($_POST['altap'])) {

				header("Location:altap.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:index.php");
			}

		}

		if ($_SESSION['rol']=='Paciente') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu4'], $_SESSION['pass4'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT pacNombres,pacApellidos FROM pacientes WHERE dniPac='".$_SESSION['nif']."';";
			$result = mysqli_query ($conexion, $sql);

			while ($registro = mysqli_fetch_row($result)) {
			   $_SESSION['nom']=$registro[0];
			   $_SESSION['apell']=$registro[1];
			}

	?>

	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><img src="atendida.png" width="64" alt="Icono citas"><br/><button type="submit" name="vercitas">Ver todas las citas</button></div>
			<div><img src="logout.png" width="64" alt="Icono cerrar sesión"><br/><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>	

	<?php

			if (isset($_POST['vercitas'])) {

				header("Location:verCitas.php");

			}

			if (isset($_POST['logout'])) {

					session_destroy();
			 
					header("Location:index.php");
			}

		}
	}
	mysqli_close($conexion);
	?>
</body>
</html>