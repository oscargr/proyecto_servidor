<?php

session_start();

$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Citas pendientes de <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST" name="miForm">
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Consultorio</th>
			<th>Atender</th>
		</tr>

		<?php

		$nif=$_SESSION['nif'];

		$sql="SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, consultorios.conNombre, pacientes.dniPac FROM citas, pacientes, consultorios WHERE citas.citMedico='$nif' AND citas.citEstado='Asignado' AND citas.citPaciente=pacientes.dniPac AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {
				
		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><button type="submit" name="ac[]" value=<?php echo $registro[5].",".$registro[0].",".$registro[1]; ?>><img src="atender.png" width="64" alt="Icono atender cita"><br/></button></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No hay ninguna cita que atender</td></tr>";
		}

		?>

	</table>
	</form>

	<?php

	if (isset($_POST['ac'])) {
		foreach ($_POST['ac'] as $value) {
			$pac=explode(",", $value);
		}
		$_SESSION['atenderpaciente']=$pac;
		var_dump($_SESSION['atenderpaciente']);
		header("Location:atenderCita.php");
	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	mysqli_close($conexion);

	?>
</body>
</html>