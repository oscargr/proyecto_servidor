<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Alta paciente</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	<h2>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST" name="altaForm" onsubmit="return validar()" class="altas-citas">
		<fieldset>
			<legend>Dar de alta a un paciente</legend>
			<p>DNI: <input type="text" name="dni" id="nif" required="required" onblur="valdni()" maxlength="10" placeholder="Ej: 12345678A"><span id="avisodni"></span></p>
			<p>Nombre: <input type="text" name="nombre" required="required" maxlength="50"></p>
			<p>Apellidos: <input type="text" name="apell" required="required" maxlength="50"></p>
			<p>Fecha de Nacimiento: <input type="date" name="fechanac" id="nacimiento" required="required" onblur="valfn()"><span id="avisofecha"></span></p>
			<p>Sexo: <select name="sexo" required="required">
				<option value="Femenino">Femenino</option>
				<option value="Masculino">Masculino</option>
			</select></p>
			<p>Usuario: <input type="text" name="usu" required="required" maxlength="15"></p>
			<p>Contraseña: <input type="password" name="pass" required="required" maxlength="157"></p>
			<p>Repita la contraseña: <input type="password" name="rpass" id="rpasswd" required="required" maxlength="157" onblur="valpass()"><span id="avisopass"></span></p>
			<p><input type="submit" name="insertar" value="Dar de alta"></p>
		</fieldset>
	</form>

	<?php

	if (isset($_POST['insertar'])) {
		$dni=$_POST['dni'];
		$nom=$_POST['nombre'];
		$apell=$_POST['apell'];
		$fn=$_POST['fechanac'];
		$sexo=$_POST['sexo'];
		$usu=$_POST['usu'];
		$pass=$_POST['pass'];

		if ($_SESSION['rol']=='Administrador') {

			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="INSERT INTO pacientes (dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo) VALUES ('$dni','$nom','$apell','$fn','$sexo');";
			$sql2="INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$dni','$usu','$pass','Activo','Paciente');";
			if (mysqli_query($conexion, $sql) && mysqli_query($conexion, $sql2)) {
			 	$mensajeregistro = "Se ha registrado el paciente con éxito";
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php		 	
			}
			else {
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
				echo " <br> Error: " . $sql2 . "<br>" . mysqli_error($conexion);
			}
		}
		if ($_SESSION['rol']=='Asistente') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="INSERT INTO pacientes (dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo) VALUES ('$dni','$nom','$apell','$fn','$sexo');";
			$sql2="INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$dni','$usu','$pass','Activo','Paciente');";
			if (mysqli_query($conexion, $sql) && mysqli_query($conexion, $sql2)) {
			 	$mensajeregistro = "Se ha registrado el paciente con éxito";
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php
			}
			else {
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
				echo " <br> Error: " . $sql2 . "<br>" . mysqli_error($conexion);
			}
		}
		mysqli_close($conexion);
	}


	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	?>

	<script>

		var modal = document.getElementById('modalB');

		window.onclick = function(event) {
		  if (event.target == modal) {
		    modal.style.display = "none";
		  }
		}

		function validar() {
			if (valdni() && valfn() && valpass()) {
				return true;
			}
			else {
				alert ("Datos erróneos, indtroducir de nuevo");
				return false;
			}
		}

		function valdni() {
			var nif = document.altaForm.dni.value;
			var expresion_regular_dni
		 
		  	expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
		 
		  	if (expresion_regular_dni.test (nif) == true) {
		  		document.getElementById('nif').style.border="3px solid green";
		    	document.getElementById('avisodni').innerHTML=" &check; DNI correcto";
		    	return true;
		  	}
		  	else {
		  		document.getElementById('nif').style.border="3px solid red";
		  		document.getElementById('avisodni').innerHTML=" &cross; DNI erróneo, formato no válido";
		  		return false;
		   	}	
		}

		function valfn() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			if (dd<10) {
				dd='0'+dd;
			}
			if (mm<10) {
				mm='0'+mm;
			}
			today = yyyy+"-"+mm+"-"+dd;

			var fecha = document.altaForm.fechanac.value;

			var cf=fecha.localeCompare(today);

			if (cf==1) {
				document.getElementById('nacimiento').style.border="3px solid red";
				document.getElementById('avisofecha').innerHTML=" &cross; Fecha incorrecta (ha de ser una fecha anterior a la de hoy)";
				return false;
			}

			else {
				document.getElementById('nacimiento').style.border="3px solid green";
				document.getElementById('avisofecha').innerHTML=" &check; Fecha correcta";
				return true;
			}
		}

		function valpass() {
			var contra=document.altaForm.pass.value;
			var rcontra=document.altaForm.rpass.value;

			if (contra===rcontra) {
				document.getElementById('rpasswd').style.border="3px solid green";
				document.getElementById('avisopass').innerHTML=" &check; Contraseña correcta";
				return true;
			}
			else {
				document.getElementById('rpasswd').style.border="3px solid red";
				document.getElementById('avisopass').innerHTML=" &cross; Contraseña incorrecta";
				return false;
			}
		}
	</script>
</body>
</html>