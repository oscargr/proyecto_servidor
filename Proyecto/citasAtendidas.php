<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Citas atendidas <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	
	<?php

	if ($_SESSION['rol']=='Medico') {

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<form action="" method="POST" name="miForm">
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
			<th>Asignar cita</th>
		</tr>

		<?php

		$nif=$_SESSION['nif'];

		$sql="SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, consultorios.conNombre, citas.citObservaciones, citas.idCita FROM citas, pacientes, consultorios WHERE citas.citMedico='$nif' AND citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {
				
		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><?php echo $registro[5]; ?></td>
			<td><button type="submit" name="ac[]" value=<?php echo $registro[6]; ?>>&check;</button></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='6'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>
	</form>

	<?php

		if (isset($_POST['ac'])) {
			$arr=$_POST['ac'];
			$idc=implode("", $arr);
			$sql2="UPDATE citas SET citEstado='Asignado', citObservaciones='' WHERE idCita='$idc';";
			if (mysqli_query($conexion, $sql2)) {
				 $mensajeregistro="Se han registrado los cambios con éxito";

	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php

				header("Refresh:2; url=citasAtendidas.php", true);
			}
			else {
				echo " <br> Error: " . $sql2 . "<br>" . mysqli_error($conexion);
			}			 
		}		
	}

	if ($_SESSION['rol']=='Asistente') {

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Médico</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$sql="SELECT citas.citFecha,citas.citHora,pacientes.pacNombres,pacientes.pacApellidos,medicos.medNombres,medicos.medApellidos,consultorios.conNombre,citas.citObservaciones FROM citas,pacientes,medicos,consultorios WHERE citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citMedico=medicos.dniMed AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {

		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]." ".$registro[5]; ?></td>
			<td><?php echo $registro[6]; ?></td>
			<td><?php echo $registro[7]; ?></td>
		</tr>	

		<?php

			}
		}
		else {
			echo "<tr><td colspan='6'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>	

	<?php	

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	mysqli_close($conexion);
	?>
</body>
</html>