# Privilegios para `acceso`@`localhost`

GRANT USAGE ON *.* TO 'acceso'@'localhost' IDENTIFIED BY PASSWORD '*EAF6366D40E7678E88E492DBE26F434826AFA83B';

GRANT SELECT (usutipo, usuLogin, dniUsu, usuPassword) ON `consultas`.`usuarios` TO 'acceso'@'localhost';


# Privilegios para `admin`@`localhost`

GRANT USAGE ON *.* TO 'admin'@'localhost' IDENTIFIED BY PASSWORD '*4ACFE3202A5FF5CF467898FC58AAB1D615029441';

GRANT ALL PRIVILEGES ON `consultas`.* TO 'admin'@'localhost' WITH GRANT OPTION;


# Privilegios para `asistente`@`localhost`

GRANT USAGE ON *.* TO 'asistente'@'localhost' IDENTIFIED BY PASSWORD '*4963DD1E44AE96E982437A04E131AA894BDBADFE';

GRANT SELECT ON `consultas`.`medicos` TO 'asistente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`pacientes` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`citas` TO 'asistente'@'localhost';

GRANT INSERT ON `consultas`.`usuarios` TO 'asistente'@'localhost';


# Privilegios para `medico`@`localhost`

GRANT USAGE ON *.* TO 'medico'@'localhost' IDENTIFIED BY PASSWORD '*98D53BF578FEB432F045DF1D3060845370BFC537';

GRANT SELECT ON `consultas`.`consultorios` TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`medicos` TO 'medico'@'localhost';

GRANT SELECT, UPDATE ON `consultas`.`citas` TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`pacientes` TO 'medico'@'localhost';


# Privilegios para `paciente`@`localhost`

GRANT USAGE ON *.* TO 'paciente'@'localhost' IDENTIFIED BY PASSWORD '*14F57843D3EDA0C1BAAE3AAA7C6CF4B627ECB763';

GRANT SELECT ON `consultas`.`medicos` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`pacientes` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`citas` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'paciente'@'localhost';