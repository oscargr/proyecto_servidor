<!DOCTYPE html>
<html lang="es">
<head>
	<title>Acceso MediCare</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	<div class="login">
		<form action="" method="POST">
			<fieldset>
				<legend>Inicio de sesión</legend>
				<p><input placeholder="Usuario" type="text" name="user" required="required"></p>
				<p><input placeholder="Contraseña" type="password" name="pass" required="required"></p>
				<p><input type="submit" name="en" value="Entrar" class="entrar"></p>
			</fieldset>
		</form>
	</div>

	<?php

	$conexion = mysqli_connect('localhost', 'acceso', 'acceso', 'consultas');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}

	if (isset($_POST['en'])) {
		$usuario=$_POST['user'];
		$password=$_POST['pass'];
		$sql = "SELECT dniUsu,usutipo FROM usuarios WHERE usuLogin = '$usuario' AND usuPassword = '$password'";
		$result = mysqli_query ($conexion, $sql);

		if(mysqli_num_rows($result) > 0) {
			while ($registro = mysqli_fetch_row($result)) {
				$nif=$registro[0];
                $rol=$registro[1];
        	}

	        session_start();

	     	$_SESSION['nif']="$nif";
	        $_SESSION['rol']="$rol";
	        $_SESSION['user']=$usuario;

	        header("Location: inicio.php");

        	exit();
		}
		else {
			$mensajeaccesoincorrecto = "<p>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
    ?>

    <div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<span onclick="document.getElementById('modalB').style.display='none'"class="botonModal grande arribaDerecha">&times;</span>
    		<div class="contenedor">
    			<p><?php echo $mensajeaccesoincorrecto; ?></p>
    		</div>
    	</div>
    </div>

	<script>
	var modal = document.getElementById('modalB');

	window.onclick = function(event) {
	  if (event.target == modal) {
	    modal.style.display = "none";
	  }
	}
	</script>

    <?php
		}
	}
	mysqli_close($conexion);

	?>
</body>
</html>