<?php

session_start();

$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

$nif=$_SESSION['atenderpaciente'][0];
$fecha=$_SESSION['atenderpaciente'][1];
$hora=$_SESSION['atenderpaciente'][2];	

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Atender cita</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>
	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú anterior</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST" class="atenderCita">
		<fieldset>
			<legend>Atender cita</legend>
			<table border="1" style="text-align: center;">
				<tr>
					<th>DNI paciente</th>
					<td><?php echo $nif; ?></td>
				</tr>
				<tr>
					<th>Nombre paciente</th>
					<?php

					$sql="SELECT pacNombres,pacApellidos FROM pacientes WHERE dniPac='$nif';";
					$result = mysqli_query ($conexion, $sql);
					$registro=mysqli_fetch_row($result);


					?>
					<td><?php echo $registro[0]." ".$registro[1]; ?></td>
				</tr>
				<tr>
					<th>Fecha cita</th>
					<td><?php echo $fecha; ?></td>
				</tr>
				<tr>
					<th>Hora cita</th>
					<td><?php echo $hora; ?></td>
				</tr>
				<tr>
					<th>Observaciones</th>
					<td><textarea name="obs" placeholder="Escriba aquí las observaciones del paciente" style="box-sizing: border-box; width: 350px; height: 200px; resize: none; overflow: auto;" required="required"></textarea></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="atender" value="Enviar"></td>
				</tr>
			</table>
		</fieldset>
	</form>

	<?php

	if (isset($_POST['atender'])) {
		$observaciones=$_POST['obs'];
		$sql="UPDATE citas SET citEstado='Atendido', CitObservaciones='$observaciones' WHERE citPaciente='$nif' AND citFecha='$fecha' AND citHora='$hora';";
		if (mysqli_query($conexion, $sql)) {
			 	$mensajeregistro="Se han registrado las observaciones con éxito, redirigiéndole a la página anterior";
	?>
	
	<div id="modalB" style="display: block;" class="modal opacidad">
    	<div class="modal-cont cajaModal">
    		<div class="contenedor">
    			<p><?php echo $mensajeregistro; ?></p>
    		</div>
    	</div>
    </div>

	<?php		 	
			 	header("Refresh:3; url=citasPendientes.php", true);
			}
		else {
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
	}

	if (isset($_POST['back'])) {

		header("Location:citasPendientes.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	mysqli_close($conexion);

	?>
</body>
</html>