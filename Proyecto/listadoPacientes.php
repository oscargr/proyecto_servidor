<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Listado de pacientes <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Girassol|Varela+Round&display=swap');
	</style>
</head>
<body>
	<header style="background-color: #05668D;">
		<img src="Logo.png" alt="Logo MediCare">
		<h1>Tu centro médico de confianza</h1>
	</header>

	<?php

	if ($_SESSION['rol']=='Medico') {

	?>

	<h2>Bienvenido/a <?php echo $_SESSION['nom']." ".$_SESSION['apell']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>
	
	<?php

		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>DNI</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Sexo</th>
		</tr>

	<?php

	$nif = $_SESSION['nif'];

	$sql="SELECT DISTINCT pacientes.* FROM pacientes,citas,medicos WHERE citas.citPaciente=pacientes.dniPac AND citas.citMedico='$nif';";
	$result = mysqli_query ($conexion, $sql);
	$filas=mysqli_num_rows($result);
	if ($filas>0) {
		while ($registro = mysqli_fetch_row($result)) {

		?>
	
		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No tiene ningún paciente, ".$_SESSION['nom']." ".$_SESSION['apell']."</td></tr>";
		}

		?>

	</table>

	<?php					

	}

	if ($_SESSION['rol']=='Asistente') {
	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<div class="backClose">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="logout">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>DNI</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Sexo</th>
		</tr>

		<?php

		$sql="SELECT * FROM pacientes;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {

		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No hay pacientes en el registro</td></tr>";
		}

		?>

	</table>	

	<?php

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['logout'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	mysqli_close($conexion);

	?>

</body>
</html>