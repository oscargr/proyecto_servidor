<?php

session_name("e3");
session_start();

if(!isset($_SESSION['x']) || !isset($_SESSION['y'])) {
	$_SESSION['x']=$_SESSION['y']=0;
}

if(!isset($_SESSION['x2']) || !isset($_SESSION['y2'])) {
	$_SESSION['x2']=$_SESSION['y2']=0;
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 3</title>
	<meta charset="utf-8">
</head>
<body>
	<h1>Mover un punto en dos dimensiones</h1>
	<p>Haga click en los botones para mover el punto:</p>
	<form action="E3_control.php" method="POST">
		<table style="text-align: center; padding-left: 70px;">
			<tr>
				<td colspan="3">
					<button type="submit" name="mov" value="arr">Arriba</button>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" name="mov" value="izq">Izquierda</button>
				</td>
				<td>
					<button type="submit" name="mov" value="cen">Volver al centro</button>
				</td>
				<td>
					<button type="submit" name="mov" value="der">Derecha</button>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<button type="submit" name="mov" value="abj">Abajo</button>
				</td>
			</tr>
		</table>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="400" height="400" viewbox="-200 -200 400 400" style="border: 2px solid black;">

	<?php

		print "<circle cx=".$_SESSION['iv']." cy=".$_SESSION['ih']." r=\"8\" fill=\"tomato\" />\n";
		print "<circle cx=".$_SESSION['x']." cy=".$_SESSION['y']." r=\"8\" fill=\"red\" />\n";
		

	?>

		</svg>
	</form>

	<?php


	var_dump($_SESSION["ih"]." ".$_SESSION["iv"]);

	?>

</body>
</html>