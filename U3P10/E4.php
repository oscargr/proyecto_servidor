<?php

session_name("e4");
session_start();

if(!isset($_SESSION['a']) || !isset($_SESSION['b'])) {
	$_SESSION['a']=$_SESSION['b']=0;
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 4</title>
	<meta charset="utf-8">
</head>
<body>
	<h1>Votar una opción</h1>
	<p>Haga click en los botones para votar por una opción:</p>
	<form action="E4_control.php" method="POST">
	<table>
		<tr>
			<td style="vertical-align: top;">
				<button type="submit" name="vot" value="a" style="font-size: 60px; line-height: 50px; color: hsl(200, 100%, 50%); cursor: pointer;">&check;</button>
			</td>
			<td>
				
	<?php

				echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" \n";
				echo "width=".$_SESSION['a']." height=\"50\">\n";
				echo "<line x1=\"0\" y1=\"25\" x2=".$_SESSION['a']." y2=\"25\" stroke=\"hsl(200, 100%, 50%)\" stroke-width=\"50\" /> \n";
				echo "<text x=0 y=30 fill=black>\n";
				echo $_SESSION['a']."</text>\n";
				echo "</svg>\n";

	?>

			</td>
		</tr>
		<tr>
			<td style="vertical-align: top;">
				<button type="submit" name="vot" value="b" style="font-size: 60px; line-height: 50px; color: hsl(35, 100%, 50%); cursor: pointer;">&check;</button>
			</td>
			<td>
				
	<?php

				echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" \n";
				echo "width=".$_SESSION['b']." height=\"50\">\n";
				echo "<line x1=\"0\" y1=\"25\" x2=".$_SESSION['b']." y2=\"25\" stroke=\"hsl(35, 100%, 50%)\" stroke-width=\"50\" /> \n";
				echo "<text x=0 y=30 fill=black>\n";
				echo $_SESSION['b']."</text>\n";
				echo "</svg>\n";

	?>

			</td>
		</tr>
		<tr>
			<td>
				<button type="submit" name="vot" value="cero" style="cursor: pointer;">Poner a cero</button>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>