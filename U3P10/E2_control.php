<?php

session_name("e2");
session_start();

if (!isset($_SESSION['pos'])) {
	$_SESSION['pos']=300;
}

function recoge($var) {
	if (isset($_REQUEST[$var])) {
		$tmp=trim (htmlspecialchars($_REQUEST[$var]));
	}
	else {
		$tmp="";
	}
	return $tmp;
}

$accion=recoge("mov");
$accionOK=false;

if ($accion!="izq" && $accion!="der" && $accion!="cen") {
	header("Location:E2.php");
	exit;
}
else {
	$accionOK=true;
}

if ($accionOK) {
	if ($accion=="cen") {
		$_SESSION['pos']=300;
	}
	elseif ($accion=="izq") {
		$_SESSION['pos']-=20;
	}
	elseif ($accion=="der") {
		$_SESSION['pos']+=20;
	}

	if ($_SESSION['pos']>600) {
		$_SESSION['pos']=0;
	}
	elseif ($_SESSION['pos']<0) {
		$_SESSION['pos']=600;
	}

	header("Location:E2.php");
	exit;
}
?>