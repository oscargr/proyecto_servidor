<?php

session_name("e4");
session_start();

if(!isset($_SESSION['a']) || !isset($_SESSION['b'])) {
	$_SESSION['a']=$_SESSION['b']=0;
}

function recoge($var) {
	if (isset($_REQUEST[$var])) {
		$tmp=trim (htmlspecialchars($_REQUEST[$var]));
	}
	else {
		$tmp="";
	}
	return $tmp;
}

$accion=recoge("vot");
$accionOK=false;

if ($accion!="a" && $accion!="b" && $accion!="cero") {
	header("Location:E4.php");
	exit;
}
else {
	$accionOK=true;
}

if ($accionOK) {

	if ($accion=="a") {
		$_SESSION['a']+=10;
	}
	elseif ($accion=="b") {
		$_SESSION['b']+=10;
	}
	elseif ($accion=="cero") {
		$_SESSION['a']=$_SESSION['b']=0;
	}

	header("Location:E4.php");
}

?>