<?php

session_name("e1");
session_start();

if (!$_SESSION['num']) {
	$_SESSION['num']=0;
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 1</title>
	<meta charset="utf-8">
</head>
<body>
	<form action="E1_control.php" method="POST">
		<p>
			<button type="submit" name="mod" value="bajar" style="font-size: 4rem;">-</button>

	<?php

			echo "<span style='font-size: 4rem'>$_SESSION[num]</span>";

	?>

			<button type="submit" name="mod" value="subir" style="font-size: 4rem;">+</button>
		</p>
		<button type="submit" name="mod" value="cero">Poner a cero</button>
	</form>
</body>
</html>