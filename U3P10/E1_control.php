<?php

session_name("e1");
session_start();

if (!isset($_SESSION['num'])) {
	$_SESSION['num']=0;
}

//$valor=$_POST['mod'];

//var_dump($valor);

function recoge($var) {
	if (isset($_REQUEST[$var])) {
		$tmp=trim(htmlspecialchars($_REQUEST[$var]));
	}
	else {
		$tmp="";
	}
	return $tmp;
}

$accion=recoge("mod");
$accionOK=false;

if ($accion!="cero" && $accion!="subir" && $accion!="bajar") {
	header("Location:E1.php");
	exit;
}

else {
	$accionOK=true;
}

if ($accionOK) {
	if ($accion=="cero") {
		$_SESSION['num']=0;
	}
	elseif ($accion=="subir") {
		$_SESSION['num']++;
	}
	elseif ($accion=="bajar") {
		$_SESSION['num']--;
	}

	header("Location: E1.php");
	exit;
}
?>