<?php

session_name("e3");
session_start();

if(!isset($_SESSION['x']) || !isset($_SESSION['y'])) {
	$_SESSION['x']=$_SESSION['y']=0;
}

function recoge($var) {
	if (isset($_REQUEST[$var])) {
		$tmp=trim (htmlspecialchars($_REQUEST[$var]));
	}
	else {
		$tmp="";
	}
	return $tmp;
}

$accion=recoge("mov");
$accionOK=false;

if ($accion!="izq" && $accion!="der" && $accion!="cen" && $accion!="arr" && $accion!="abj") {
	header("Location:E3.php");
	exit;
}

else {
	$accionOK=true;
}

if ($accionOK) {
	switch ($accion) {
		case 'cen':
			$_SESSION['x']=0;
			$_SESSION['y']=0;
			break;
		case 'arr':
			$_SESSION['y']-=20;
			$_SESSION['ih']=$_SESSION['y'];
			$_SESSION['iv']=$_SESSION['x'];
			break;
		case 'abj':
			$_SESSION['y']+=20;
			$_SESSION['ih']=$_SESSION['y'];
			$_SESSION['iv']=$_SESSION['x'];
			break;
		case 'izq':
			$_SESSION['x']-=20;
			$_SESSION['ih']=$_SESSION['y'];
			$_SESSION['iv']=$_SESSION['x'];
			break;
		case 'der':
			$_SESSION['x']+=20;
			$_SESSION['ih']=$_SESSION['y'];
			$_SESSION['iv']=$_SESSION['x'];	
			break;	
	}

	if ($_SESSION['x']>200) {
		$_SESSION['x']=-200;
		$_SESSION['x2']=-200;
	}
	elseif ($_SESSION['x']<-200) {
		$_SESSION['x']=200;
		$_SESSION['x2']=200;
	}

	if ($_SESSION['y']>200) {
		$_SESSION['y']=-200;
		$_SESSION['y2']=-200;
	}
	elseif ($_SESSION['y']<-200) {
		$_SESSION['y']=200;
		$_SESSION['y2']=200;
	}

	header("Location:E3.php");
}

?>