<?php

session_name("e2");
session_start();

if(!isset($_SESSION['pos'])) {
	$_SESSION['pos']=300;
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
</head>
<body>
	<h1 style="text-align: center;">Mover un punto a derecha e izquierda</h1>
	<p>Haga click en los botones para mover el punto:</p>
	<form action="E2_control.php" method="POST">
		<p style="padding-left: 228px;">
			<button type="submit" name="mov" value="izq">Izquierda</button>
			<button type="submit" name="mov" value="der">Derecha</button>
		</p>
		<svg height="30" width="1000">
  			<line x1="-600" y1="10" x2="600" y2="10" style="stroke:rgb(0,0,0);stroke-width:5" />

  	<?php		
  			
  			print "<circle cx=".$_SESSION['pos']." cy=\"10\" r=\"8\" fill=\"red\" />\n";

  	?>		
		</svg>
		<p style="padding-left: 247px;">
			<button type="submit" name="mov" value="cen">
				Volver al centro
			</button>
		</p>
	</form>
</body>
</html>