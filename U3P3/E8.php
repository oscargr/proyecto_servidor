<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 8</title>
	<meta charset="utf-8">
</head>
<body>
	<p>Una expresión regular o regex es un patrón que se compara con una cadena objetivo de izquierda a derecha, carácter a carácter. La biblioteca PCRE (Perl Compatible Regular Expressions) es una extensión incorporada en PHP que permite utilizar expresiones regulares en funciones para buscar, comparar y sustituir strings en PHP.</p>
</body>
</html>