<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 1</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	echo "<p>La función trim() elimina los caracteres del inicio y el final de la cadena de texto</p>";

	$texto = "Hola mundo";

	echo "<p>Variable \$texto: $texto</p>";

	$result = trim($texto, "Hoa");

	echo "Texto tras el trim(\$texto, 'Hoa'): $result";

	?>
</body>
</html>