<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	echo "<p>La función stripslashes() elimina las contrabarras (utilizadas de caracteres especiales) de un string";

	$texto = "H\$l\$ m\\nd\$";

	echo "<p>Variable \$texto: $texto</p>";

	$result = stripslashes($texto);

	echo "Texto tras el stripslashes(\$texto): $result";

	?>
</body>
</html>