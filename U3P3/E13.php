<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 13</title>
	<meta charset="utf-8">
</head>
<body>
	<form method="POST" action="<?php echo $_SERVER["PHP_SELF"];?>">
		<fieldset>
			<legend>Ejemplo de validación de formularios con PHP</legend>
			<span style="color: red;">* campo requerido</span>
			<br/><br/>
			Nombre: 
			<input type="text" name="nom" value="<?php if(isset($_POST['en'])) { echo $_POST['nom']; } ?>">
			<span style="color: red;"> * </span>
			<?php 

				if (isset($_POST['en']) && !vnom($_POST['nom'])) {
					echo "<span style='color: red;'>El nombre debe contener sólo letras y espacios en blanco</span>";
				}
				if (isset($_POST['en']) && empty($_POST['nom'])) {
					echo "<span style='color: red;'>El nombre es un campo requerido</span>";
				}

			 ?>
			<br/><br/>
			E-mail: 
			<input type="text" name="mail" value="<?php if(isset($_POST['en'])) { echo $_POST['mail']; } ?>">
			<span style="color: red;"> * </span>
			<?php 

				if (isset($_POST['en'])) {
					if (empty($_POST['mail'])) {
						echo "<span style='color: red;'>El email es un campo requerido</span>";
					}
					else {
						if (!vmail($_POST['mail'])) {
							echo "<span style='color: red;'>El formato de email es inválido</span>";
						}
					}
				}

			 ?>
			<br/><br/>
			Sitio web: 
			<input type="text" name="url" value="<?php if(isset($_POST['en'])) { echo $_POST['url']; } ?>">
			<?php 

				if (isset($_POST['en']) && !vurl($_POST['url'])) {
					echo "<span style='color: red;'>* URL Inválida</span>";
				}

			 ?>
			<br/><br/>
			Comentarios: 
			<textarea name="comentario" rows="10" cols="40">
			</textarea>
			<br/><br>
			Sexo: 
			<input type="radio" name="sexo" value="mujer"
			<?php if(isset($_POST['sexo']) && $_POST['sexo']=="mujer") { echo "checked"; } ?>> Mujer
			<input type="radio" name="sexo" value="hombre"
			<?php if(isset($_POST['sexo']) && $_POST['sexo']=="hombre") { echo "checked"; } ?>> Hombre
			<span style="color: red;"> * </span>
			<?php

				if (isset($_POST['en']) && empty($_POST['sexo'])) {
					echo "<span style='color: red;'>El sexo es un campo requerido</span>";
				}

			?>
			<br/><br/>
			<input type="submit" name="en" value="Enviar">
		</fieldset>
	</form>

	<?php

	function vnom($nom) {
		if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*$/",$nom)) {
			return false;
		}
		else {
			return true;
		}
	}

	function vmail($mail) {
		if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			return false;
		}
		else {
			return true;
		}
	}

	function vurl($url) {
		if (!filter_var($url, FILTER_VALIDATE_URL)) {
			return false;
		}
		else {
			return true;
		}
	}

	if (isset($_POST['en'])) {
		$nombre = $_POST['nom'];
		$mail = $_POST['mail'];
		$url = $_POST['url'];
		$comentario = $_POST['comentario'];
		if (isset($_POST['sexo'])) {
			$sexo = $_POST['sexo'];
		}
		else {
			$sexo="";
		}

		echo "<h2>Tus datos:</h2>";

		$datos = array("Nombre" => $nombre,"Email" => $mail,"Sitio web" => $url,"Comentarios" => $comentario,"Sexo" => $sexo);

		foreach ($datos as $i => $v) {
			if (!empty($v)) {
				echo "<p>$i: $v</p>";
			}
		}
	}

	?>
</body>
</html>