<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 3</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	function test_input($data) {
		$data = trim($data, "o");
		$data = stripslashes($data);
		return $data;
	}

	echo "<p>La función anterior a partir de la cadena que se le pasa a la función elimina los primeros y últimos caracteres de la cadena y elimina las contrabarras que puedan existir dentro de ésta</p>";

	$texto = "H\\'ola \$\\'o\\' mund\\'o";

	echo "<p>Variable \$texto: $texto</p>";

	echo "Resultado: ".test_input($texto);

	?>
</body>
</html>