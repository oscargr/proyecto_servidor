<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 6</title>
	<meta charset="utf-8">
</head>
<body>
	<?php
	$email="abc@abc.com";
	$emailErr="Email correcto";
	if (empty($email)) {
		$emailErr = "Se requiere Email";
	} 
	else {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$emailErr = "Fomato de Email invalido";
		}
	}
	echo $email;
	echo "<br>";
	echo $emailErr;
	?>

	<p>El código de PHP se pasa un email y se da el mensaje de email correcto por defecto, si el email está vacío nos aparece un error que pide el email, sino si el email es disinto del filtro de email que se ha creado da el mensaje de formato de email inválido. Finalmente, muestra el email y el mensaje que coresponda.</p>

	<p>filter_var filtra una variable con el filtro que se indique, los parámetros que se le pasan son la variable a filtrar y el filtro que se elija de entre los tipos de filtros del manual.</p>
</body>
</html>