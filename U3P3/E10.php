<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 10</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	if (empty($_POST["name"])) {
		$nameErr = "Name is required";
	} else {
		$name = test_input($_POST["name"]);
		// check if name only contains letters and whitespace
		if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
			$nameErr = "Only letters and white space allowed";
		}
	}

	?>

	<p>Si el input del formulario name está vacío el mensaje que se guarda es que se necesita name, sino en la variable name se guarda el input filtrado por una función que usa trim y stripslashes y finalmente con una expresión regular se comprueba que el nombre no incluya nin números ni otros caracteres que no sean letras o espacios en blanco.</p>

	<p>preg_match comapara una expresión regular con un string, en este caso los parámetros que se le pasan son el patrón y la cadena que se quiere comparar con éste, pero hay más parámetros.</p>
</body>
</html>