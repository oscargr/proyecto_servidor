<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //En este caso no hay que hacer nada porque sigue la nomenclatura de Laravel de tabla plural minusculas y objeto singular primera mayúscula, si no se sigue esa nomeclatura aquí se definirá la relación de objeto y tabla
}
