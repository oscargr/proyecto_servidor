<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('inicio', array('nombre'=>'Óscar', 'anios'=>24));
});*/

/*Route::get('login', function() {
	return ('Login de usuario');
});

Route::get('logout', function() {
	return ('Logout de usuario');
});*/

Route::get('/', 'HomeController@getHome');

Route::group(['middleware' => 'auth'], function() {

Route::get('catalog', 'CatalogController@getIndex');

Route::get('catalog/show/{id}', 'CatalogController@getShow');

Route::put('catalog/show/{id}', 'CatalogController@putAlquilaDevuelve');

Route::get('catalog/create', 'CatalogController@getCreate');

Route::post('catalog/create', 'CatalogController@postCreate');

Route::get('catalog/edit/{id}', 'CatalogController@getEdit');

Route::put('catalog/edit/{id?}', 'CatalogController@putEdit');

Route::get('catalog/delete/{id}', 'CatalogController@getDelete');

Route::delete('catalog/delete/{id}', 'CatalogController@deleteMovie');

Route::get('catalog/byyear', 'CatalogController@movieYears');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
