@extends('layouts.master')

@section('content')

	<div class="row">
		<div class="col">
			<div style="display: flex; flex-flow: row wrap; justify-content: space-around;">
			<?php
			foreach ($arrayPeliculas as $pelicula) {
				echo "<div style='width: 350px; text-align: center; margin: 20px 0px;'>";
			?>
					<p><a href="{{url('catalog/show/'.$pelicula->id)}}" title="Ir a la ficha de '{{$pelicula->title}}'"><img height="250" src=<?php echo $pelicula->poster;?>></a><p>
					<p><a href="{{url('catalog/show/'.$pelicula->id)}}" title="Ir a la ficha de '{{$pelicula->title}}'"><?php echo $pelicula->title; ?></a></p>
			<?php
				echo "</div>";
			}
			?>
			</div>
		</div>
	</div>

@stop