@extends('layouts.master')

@section('content')

<h2 class="text-center">Eliminar "<i><?php echo $pelicula->title; ?></i>"</h2>
<div class="card text-center">
	<img class="card-img-top w-100" src="{{$pelicula->poster}}" alt="Poster {{$pelicula->poster}}" height="300">
	<div class="card-body">
		<h3>{{$pelicula->title}} ({{$pelicula->year}})</h3>
		<p>Dirigida por <strong>{{$pelicula->director}}</strong></p>
	</div>
	<div class="card-footer">
		<h4>¿Desea eliminar la película?</h4>
		<form action="{{action('CatalogController@deleteMovie', $pelicula->id)}}" method="POST"  style="width: 50%; margin: 0px auto;">
			<input type="hidden" name="_method" value="DELETE">
   			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="submit" class="btn btn-success" value="Confirmar">
			<a href="{{url('catalog/show/'.$pelicula->id)}}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>

@stop