@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col">
		<table class="table table-dark table-striped">
			<thead class="bg-dark">
				<tr>
					<th>Película más nueva</th>
					<th>Película más antigua</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{$max[0]->title}}</td>
					<td>{{$min[0]->title}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col">
		<?php foreach ($peliculas as $pelicula): $anio = ($pelicula->year-1900)*5; ?>
			<div style="margin: 50px 0px;">
				<h2>{{$pelicula->title}}</h2>
				<svg width="600" height="20">
					<rect style="fill: #337AB7" x="0" y="0" height="20" width="{{$anio}}"></rect>
				</svg>
				<span><strong>{{$pelicula->year}}</strong></span>
			</div>
		<?php endforeach ?>
	</div>
</div>

@stop