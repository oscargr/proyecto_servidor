@extends('layouts.master')

@section('content')

<table class="text-center" style="width: 80%; margin: 0px auto;">
	<tr>
		<td rowspan="3">
			<img src="<?php echo $pelicula->poster; ?>" alt="Poster de <?php echo $pelicula->title; ?>" height="500">
		</td>
		<td>
			<h2><?php echo $pelicula->title." (".$pelicula->year.")"; ?></h2>
		</td>
	</tr>
	<tr>
		<td>
			<p>Dirigida por <strong><?php echo $pelicula->director; ?></strong></p>
		</td>
	</tr>
	<tr>
		<td class="text-left" style="padding: 20px;">
			<p><?php echo $pelicula->synopsis; ?></p>
			<p>
				<a href="{{url('catalog/edit/'.$pelicula->id)}}" class="btn btn-warning mx-3" style="margin: 5px 5px;"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>  Editar película</a>
				<a href="{{url('catalog/delete/'.$pelicula->id)}}" class="btn btn-danger mx-3" style="margin: 5px 5px;"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>  Eliminar película</a>
				<a href="{{url('catalog')}}" class="btn btn-primary mx-3" style="margin: 5px 5px;"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>  Volver al catálogo</a>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top: 20px;">
			<form action="{{action('CatalogController@putAlquilaDevuelve', $pelicula->id)}}" method="POST">
				<input type="hidden" name="_method" value="PUT">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<?php if ($pelicula->rented==0) { ?>
				<button type="submit" class="btn btn-success" style="width: 100%;"><span class="glyphicon glyphicon-download" aria-hidden="true"></span>  Alquilar</button>
				<?php } else { ?>
				<button type="submit" class="btn btn-danger" style="width: 100%;"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span>  Devolver pelicula</button>
				<?php } ?>
			</form>
		</td>
	</tr>
</table>

@stop