<!DOCTYPE html>
<html lang="es">
<head>
	<title>Inicio tienda</title>
	<meta charset="utf-8">
	<style type="text/css">
		img, img::after, img::before {
			-webkit-user-select: none;
			-webkit-user-drag: none;
			-webkit-app-region: no-drag;
			cursor: default;
		}
	</style>
</head>
<body>
	<div class="login">
		<form action="" method="POST">
			<fieldset>
				<legend>Inicio de sesión</legend>
				<p><input placeholder="Usuario" type="text" name="user" required="required"></p>
				<p><input id="pass" placeholder="Contraseña" type="password" name="pass" required="required"><button type="button" onmousedown="mostrar()" onmouseup="ocultar()" ondrag="ocultar()" draggable="true"; style="cursor: pointer; border: none; background-color: white; outline: none;" title="Mostrar/ocultar contraseña"><img src="ojo.png" alt="Icono mostrar y ocultar contraseña" width="24"></button></p>
				<p><input type="submit" name="en" value="Iniciar sesión" class="entrar"></p>
			</fieldset>
		</form>
	</div>

	<?php

	$conexion = mysqli_connect('localhost', 'accesoventas', 'accesoventas', 'ventas');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}

	if (isset($_POST['en'])) {
		$usuario=$_POST['user'];
		$password=$_POST['pass'];
		$contraHash=hash_hmac("sha512", $password, "primeraweb", FALSE);
		$sql = "SELECT usuario,rol,password FROM usuarios WHERE usuario = '$usuario' AND password = '$contraHash'";
		$result = mysqli_query ($conexion, $sql);

		if(mysqli_num_rows($result) > 0) {
			while ($registro = mysqli_fetch_row($result)) {
				$user=$registro[0];
                $rol=$registro[1];
                $pass=$registro[2];
        	}

	        session_start();

	     	$_SESSION['usuario']="$user";
	        $_SESSION['rol']="$rol";

	        header("Location: inicio.php");
     
        	exit();
		}
		else {
			$mensajeaccesoincorrecto = "<p>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
			echo $mensajeaccesoincorrecto;
		}
	}
	mysqli_close($conexion);

	?>

	<form action="" method="POST">
		<p>Puedes registrarte aquí: <input type="submit" name="reg" value="Registro de usuario"></p>
	</form>

	<?php

	if (isset($_POST['reg'])) {

		header("Location:registro.php");

	}

	?>

	<script>

		function mostrar() {
			document.getElementById('pass').type="text";
		}

		function ocultar() {
			document.getElementById('pass').type="password";
		}

	</script>
</body>
</html>