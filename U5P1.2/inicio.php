<?php

session_start();
$_SESSION['servidor']='localhost';
$_SESSION['basedatos']='ventas';
$_SESSION['usu1']='administrador';
$_SESSION['pass1']='administrador';
$_SESSION['usu2']='consultorventas';
$_SESSION['pass2']='consultorventas';

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Inicio</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	if (isset($_SESSION['usuario']) && isset($_SESSION['rol'])) {
		if ($_SESSION['rol']=="administrador") {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><button type="submit" name="insart">Introducir nuevo artículo</button></div>
			<div><button type="submit" name="verart">Ver artículos</button></div>
			<div><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>

	<?php

			if (isset($_POST['insart'])) {

				header("Location:introducir_articulo.php");

			}

			if (isset($_POST['verart'])) {

				header("Location:ver_articulos.php");

			}

			if (isset($_POST['logout'])) {

				session_destroy();
		 
				header("Location:index.php");
			}
		}

		if ($_SESSION['rol']=="consultor") {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h2>Bienvenido/a <?php echo $_SESSION['usuario']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h2>
	<form action="" method="POST" class="menu">
			<div><button type="submit" name="verart">Ver artículos</button></div>
			<div><button type="submit" name="logout">Cerrar Sesión</button></div>
	</form>

	<?php

			if (isset($_POST['verart'])) {

				header("Location:ver_articulos.php");

			}

			if (isset($_POST['logout'])) {

				session_destroy();
		 
				header("Location:index.php");
			}		

		}
	}

	mysqli_close($conexion);

	?>
</body>
</html>