<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 13</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$arr = array("Pedro","Ana",34,1);

	echo "<h2>Mostrar array usando print_r</h2>";
	print_r($arr);
	echo "<p>Muestra el array, los índices y el valor dentro de cada índice</p>";

	echo "<h2>Mostrar array usando var_dump</h2>";
	var_dump($arr);
	echo "<p>Muestra el tipo de dato, la longitud de éste, su índice en el array y su valor</p>";

	echo "<h2>Mostrar array usando var_export</h2>";
	var_export($arr);
	echo "<p>Muestra un código PHP que puede ser implementado</p>";

	?>
</body>
</html>