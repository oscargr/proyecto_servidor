<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 14</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$datos = array(
		"Nombre" => "Pedro Torres",
		"Dirección" => "C/Mayor, 37",
		"Teléfono" => "123456789"
	);

	foreach ($datos as $dato => $v) {
		echo $dato.": ".$v."<br/><br/>";
	}

	?>
</body>
</html>