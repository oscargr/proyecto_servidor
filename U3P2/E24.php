<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 24</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$numeros = array('N1' => 3,'N2' => 2,'N3' => 8,'N4' => 123,'N5' => 5,'N6' => 1);

	asort($numeros);

	?>

	<table border="1">
		<tr>
			<th colspan="2">Números ordenados de menor a mayor</th>
		</tr>
		<tr>
			<th>Clave</th>
			<th>Valor</th>
		</tr>

	<?php

	foreach ($numeros as $clave => $num) {

	?>

		<tr>
			<td><?php echo $clave; ?></td>
			<td><?php echo $num; ?></td>
		</tr>

	<?php

	}

	?>

	</table>

</body>
</html>