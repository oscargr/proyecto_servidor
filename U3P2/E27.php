<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 27</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$deportes = array("fútbol","baloncesto","natación","tenis");

	echo "<h2>Array definido</h2>";

	for ($i=0; $i<sizeof($deportes); $i++) {
		echo $deportes[$i]." ";
	}

	echo "<h2>Total de valores en el array</h2>";

	echo count($deportes);

	$pos=0;

	$lastp=sizeof($deportes)-1;

	echo "<h2>Valor de la posición 0 del array</h2>";

	echo $deportes[$pos];

	echo "<h2>Valor de la siguiente posición</h2>";

	$pos++;

	echo $deportes[$pos];

	echo "<h2>Valor de la última posición del array</h2>";

	echo $deportes[$lastp];

	echo "<h2>Valor de la penúltima posición</h2>";

	$lastp--;

	echo $deportes[$lastp];

	?>
</body>
</html>