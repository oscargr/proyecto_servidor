<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 1</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$coches=array(32,11,45,22,78,-3,9,66,5);

	echo "<p>La posición 5 del array 'coches' es: $coches[5]</p>";

	$importe=array(32.583,11.239,45.781,22.237);

	echo "<p>La posición 1 del array 'importe' es: $importe[1]</p>";

	$confirmado=array("true","true","false","true","false","false");

	echo "<p>La posición 0 del array 'confirmado' es: $confirmado[0]</p>";

	$jugador=array("Crovic","Antic","Malic","Zulic","Rostrich");

	$cad="La alineación del equipo está compuesta por ";

	echo "<p>".$cad.$jugador[0].", ".$jugador[1].", ".$jugador[2].", ".$jugador[3]." y ".$jugador[4].".</p>";

	?>
</body>
</html>