<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 16</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$cities = array("MD" => "Madrid","BC" => "Barcelona","LD" => "Londres","NY" => "New York","LA" => "Los Ángeles","CH" => "Chicago");

	foreach ($cities as $id => $ciudad) {
		echo "El índice del array que contiene como valor ".$ciudad." es ".$id.".<br/><br/>";
	}

	?>
</body>
</html>