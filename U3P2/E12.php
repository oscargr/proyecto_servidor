<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 12</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$meses = array(
		"enero" => 9,
		"febrero" => 12,
		"marzo" => 0,
		"abril" => 17
	);

	foreach ($meses as $mes => $np) {
		if ($np!=0) {
			echo "<p>En ".$mes." ha visto ".$np." películas</p>";
		}
		/*else {
			echo "<p>En ".$mes." no ha visto ninguna película</p>";
		}*/
	}

	?>
</body>
</html>