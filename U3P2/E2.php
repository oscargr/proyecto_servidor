<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 2</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	echo "<h2>Primera forma de cargar un array</h2>";

	$arr1[0][0]=1; $arr1[0][1]=14; $arr1[0][2]=8; $arr1[0][3]=3;
	$arr1[1][0]=6; $arr1[1][1]=19; $arr1[1][2]=7; $arr1[1][3]=2;
	$arr1[2][0]=3; $arr1[2][1]=13; $arr1[2][2]=4; $arr1[2][3]=1;

	for ($i=0; $i<3; $i++) {
		for ($j=0; $j<4; $j++) {
			echo ($arr1[$i][$j]." ");
		}
		echo "<br/>";
	}

	echo "<h2>Segunda forma de cargar un array</h2>";

	$arr2 = array (
		array(1,14,8,3),
		array(6,19,7,2),
		array(3,13,4,1),
	);

	for ($i=0; $i<3; $i++) {
		for ($j=0; $j<4; $j++) {
			echo ($arr2[$i][$j]." ");
		}
		echo "<br/>";
	}

	echo "<h2>Tercera forma de cargar un array</h2>";

	$arr3[0] = array(1,14,8,3);
	$arr3[1] = array(6,19,7,2);
	$arr3[2] = array(3,13,4,1);

	for ($i=0; $i<3; $i++) {
		for ($j=0; $j<4; $j++) {
			echo ($arr3[$i][$j]." ");
		}
		echo "<br/>";
	}

	?>
</body>
</html>