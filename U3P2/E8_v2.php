<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 8_v.2</title>
	<meta charset="utf-8">
</head>
<body>
	
	<form action="" method="POST">
		<fieldset>
			<legend>Matriz 3*4</legend>
	<?php

	for ($i=0; $i<3; $i++) {
		for ($j=0; $j<4; $j++) {

	?>

			Número [<?php echo "$i"; ?>][<?php echo "$j"; ?>]: 
			<input type="text" name=<?php echo "arr".$i.$j;?>
			value = <?php 
			if (!isset($_POST['enviar'])) {
				echo rand(1,100);
			}//Si no se ha enviado el formulario genera números aleatorios
			else {
				echo $_POST["arr".$i.$j];
			}//Si se pulsó enviar se muestra en el formulario los valores que se enviaron al array
			//Con este condicional si se recarga la página sin enviar se generan valores nuevos, sino se muestran los valores enviados, si se carga la página de nuevo se genran valores nuevos
			?>>
			<br/><br/>

	<?php
	
		}
	}

	?>

			<input type="submit" name="enviar" value="Enviar">
		</fieldset>
	</form>

	<?php

	if (isset($_POST['enviar'])) {

		for ($i=0; $i < 3; $i++) { 
			for ($j=0; $j < 4; $j++) {
				$arr[$i][$j] = $_POST["arr$i$j"];
			}
		}

		echo "<h2>Array ya cargado</h2>";

		for ($i=0; $i<3; $i++) {
			for ($j=0; $j<4; $j++) {
				echo ($arr[$i][$j]." ");
			}
			echo "<br/>";
		}

		$may=0;
		$sum=0;
		for ($i=0; $i<3; $i++) {
			$may=0;
			$sum=0;
			for ($j=0; $j<4; $j++) {
				if ($arr[$i][$j]>$may) {
					$may=$arr[$i][$j];
				}
				$mayores[$i]=$may;
				$sum+=$arr[$i][$j];
			}
			$promedio[$i]=$sum/4;
		}

		echo "<h2>Valores mayores de cada fila</h2>";

		for ($i=0; $i < 3; $i++) { 
			echo $mayores[$i]." ";
		}

		echo "<h2>Valor promedio de cada fila</h2>";

		for ($i=0; $i < 3; $i++) { 
			echo $promedio[$i]." ";
		}

	}

	?>	
</body>
</html>