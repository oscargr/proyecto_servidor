<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 6</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	if (!isset($_POST['enviar'])) {

	?>
	
	<form action="" method="POST">
		<fieldset>
			<legend>Matriz 4*5</legend>
	<?php

	for ($i=0; $i<4; $i++) {
		for ($j=0; $j<5; $j++) {

	?>

			Número [<?php echo "$i"; ?>][<?php echo "$j"; ?>]: 
			<input type="text" name=<?php echo "arr".$i.$j;?>>
			<br/><br/>

	<?php
	
		}
	}

	?>

			<input type="submit" name="enviar" value="Enviar">
		</fieldset>
	</form>

	<?php

	}

	else {

		for ($i=0; $i < 4; $i++) { 
			for ($j=0; $j < 5; $j++) {
				$arr[$i][$j] = $_POST["arr$i$j"];
			}
		}

		echo "<h2>Array ya cargado</h2>";

		for ($i=0; $i<4; $i++) {
			for ($j=0; $j<5; $j++) {
				echo ($arr[$i][$j]." ");
			}
			echo "<br/>";
		}

		$may=0;

		for ($i=0; $i<4; $i++) {
			for ($j=0; $j<5; $j++) {
				if ($arr[$i][$j]>$may) {
					$may=$arr[$i][$j];
					$fila=$i;
					$columna=$j;
				}
			}
		}

		$fila++;
		$columna++;

		echo "<br/><br/>El número mayor de la matriz es $may y está en la fila $fila y en la columna $columna";

	}

	?>
</body>
</html>