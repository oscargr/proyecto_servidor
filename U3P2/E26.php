<!DOCTYPE html>
<html lang="es">
<head>
	<title>Eejrcicio 26</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$cartoonfam = array(
		array('Familia' => "Los Simpson",'padre' => "Homer",'madre' => "Marge",'hijos' => array ("Bart", "Lisa","Maggie")),
		array('Familia' => "Los Griffin",'padre' => "Peter",'madre' => "Lois",'hijos' => array ("Chris", "Meg", "Stewie"))
	);

	?>

	<ul>

	<?php
	
	foreach ($cartoonfam as $fam) {
		foreach ($fam as $tf => $nom) {
			if (!is_array($nom)) {
	
	?>

		<li><?php echo $tf.": ".$nom; ?></li>

	<?php

			}
			else {
	?>			
				<li><?php echo $tf;?>:

	<?php				
				foreach ($nom as $n) {

	?>				
					<?php echo $n;?> </li>

	<?php				
				}
			}
	
		}

	?>
		
		<br/><br/>

	<?php	

	}

	?>

	</ul>	
</body>
</html>