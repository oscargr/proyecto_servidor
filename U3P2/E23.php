<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 23</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$estadios_futbol = array('Barcelona' => "Camp Nou",'Real Madrid' => "Santiago Bernabeu",'Valencia' => "Mestalla",'Real Sociedad' => "Anoeta");

	?>

	<table border="1">
		<tr>
			<th colspan="2">Estadios de fútbol</th>
		</tr>
		<tr>
			<th>Equipo</th>
			<th>Nombre del estadio</th>
		</tr>

	<?php

	foreach ($estadios_futbol as $equipo => $estadio) {

	?>

		<tr>
			<td><?php echo $equipo; ?></td>
			<td><?php echo $estadio; ?></td>
		</tr>

	<?php

	}

	?>

	</table>

	<?php

	unset($estadios_futbol['Real Madrid']);

	?>

	<br/><br/>

	<ol>

	<?php
	
	foreach ($estadios_futbol as $equipo => $estadio) {

	?>

		<li><?php echo "El estadio ".$estadio." es del equipo ".$equipo;?></li>

	<?php
	
	}

	?>

	</ol>	

</body>
</html>