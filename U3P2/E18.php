<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 18</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$lenguajes_cliente = array('C1' => "Visual Basic Script",'C2' => "Javascript");

	$lenguajes_servidor = array('S1' => "ASP.NET",'S2' => "PERL", 'S3' => "PHP");

	$lenguajes = array_merge($lenguajes_cliente,$lenguajes_servidor);

	?>

	<table border="1">
		<tr>
			<th colspan="2">Lenguajes de programación del lado cliente y servidor</th>
		</tr>
		<tr>
			<th>Clave</th>
			<th>Nombre del lenguaje</th>
		</tr>

	<?php

	foreach ($lenguajes as $clave => $lp) {

	?>

		<tr>
			<td><?php echo $clave; ?></td>
			<td><?php echo $lp; ?></td>
		</tr>

	<?php
	
	}

	?>

	</table>	
</body>
</html>