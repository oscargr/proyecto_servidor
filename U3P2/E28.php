<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 28</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$agenda = array(
		'En Madrid' => array('nombre' => "Pedro",'edad' => "32",'Teléfono' => "91-999.99.99"),
		'En Barcelona' => array('nombre' => "Susana",'edad' => "34",'Teléfono' => "93-000.00.00"),
		'En Toledo' => array('nombre' => "Sonia",'edad' => "42",'Teléfono' => "925-09.09.09")
	);

	?>

	<ul>

	<?php
	
	foreach ($agenda as $ciudad => $amigos) {
	
	?>

		<li><?php echo $ciudad.":"; ?></li>
		<ul>

	<?php
	
		foreach ($amigos as $dato => $v) {

	?>

			<li><?php echo $dato.": ".$v; ?></li>

	<?php
	
		}

	?>
		
		</ul>

	<?php
	
	}

	?>

	</ul>	

</body>
</html>