<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 17</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$nombres = array("Pedro","Ismael","Sonia","Clara","Susana","Alfonso","Teresa");

	?>

	<p>El array contiene <?php echo count($nombres);?> nombres:</p>
	<ul>

	<?php
	
	foreach ($nombres as $ind => $nom) {

	?>

		<li><?php echo $nom." está en la posición ".$ind; ?></li>

	<?php
	
	}

	?>

	</ul>		
</body>
</html>