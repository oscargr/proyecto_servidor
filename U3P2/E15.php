<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 15</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$cities = array("Madrid","Barcelona","Londres","New York","Los Ángeles","Chicago");

	foreach ($cities as $ind => $ciudad) {
		echo "La ciudad con el índice ".$ind." tiene el nombre de ".$ciudad."<br/><br/>";
	}

	?>
</body>
</html>