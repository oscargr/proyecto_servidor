<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 9</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	for ($i=0; $i < 10; $i++) { 
		for ($j=0; $j < 10; $j++) { 
			$arr[$i][$j]=rand(1,50);
		}
	}

	$mayor=0;

	for ($i=0; $i < 10; $i++) { 
		for ($j=0; $j < 10; $j++) { 
			if ($arr[$i][$j]>$mayor) {
				$mayor=$arr[$i][$j];
				$fila=$i;
				$columna=$j;
			}
		}
	}

	$fila++;
	$columna++;

	echo "<h2>Array ya cargado</h2>";

	for ($i=0; $i<10; $i++) {
		for ($j=0; $j<10; $j++) {
			echo ($arr[$i][$j]." ");
		}
		echo "<br/>";
	}

	echo "<p>El mayor número de la matriz es $mayor y está situado en la fila $fila y en la columna $columna";

	?>
</body>
</html>