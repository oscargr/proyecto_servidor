<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 25</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$arr = array(5 => 1,12 => 2,13 => 56,'x' => 42);

	foreach ($arr as $ind => $v) {
		echo "Índice: ".$ind." Valor: ".$v."<br/><br/>";
	}

	echo "<p>El array tiene: ".count($arr)." elementos";

	unset($arr[5]);

	echo "<br/><br/><br/>Eliminando valor posición 5...<br/><br/><br/>";

	foreach ($arr as $ind => $v) {
		echo "Índice: ".$ind." Valor: ".$v."<br/><br/>";
	}

	unset($arr);

	echo "<p>Resultado de var_dump tras eliminar el array:</p>";

	var_dump($arr);

	?>
</body>
</html>