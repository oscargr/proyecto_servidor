<!DOCTYPE html>
<html lang="es">
<head>
	<title>La maldad de las 13:20</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	$v = array(
		array("Primera celda","Segunda celda",array("V121","V122","V123"),array(array("V131","V132","V133"),array("V134","V135","V136"))),

		array(array("V211","V212","V213"),"Segunda celda",array("V221","V222","V223"),array(array("V231","V232","V233"),array("V234","V235","V236"))),

		array("Primera celda","Segunda celda",array("V311","V312","V313"),array(array("V331","V332","V333"),array("V334","V335","V336")))
	);

	echo "<ul>";

	foreach ($v as $ind => $vector) {
		foreach ($vector as $ind2 => $vector1) {
			if (!is_array($vector1)) {
				echo "<li>Índice: ".$ind2." Valor: ".$vector1."</li><br/>";
			}
			else {
				echo "<li>Índice: ".$ind2."</li><br/>";
				echo "<ul>";
				foreach ($vector1 as $ind3 => $vector2) {
					if (!is_array($vector2)) {
						echo "<li>Índice: ".$ind3." Valor: ".$vector2."</li>";
					}
					else {
						foreach ($vector2 as $ind4 => $vector3) {
							echo "<li>Índice: ".$ind4." Valor:  ".$vector3."</li>";
						}
						echo "<br/>";
					}
				}
				echo "</ul>";
			}
			echo "<br/>";
		}
	}

	echo "</ul>";

	?>
</body>
</html>