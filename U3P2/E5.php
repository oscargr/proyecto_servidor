<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 5</title>
	<meta charset="utf-8">
</head>
<body>
	<?php

	if (!isset($_POST['enviar'])) {

	?>
	
	<form action="" method="POST">
		<fieldset>
			<legend>Matriz 3*5</legend>
	<?php

	for ($i=0; $i<3; $i++) {
		for ($j=0; $j<5; $j++) {

	?>

			Número [<?php echo "$i"; ?>][<?php echo "$j"; ?>]: 
			<input type="text" name=<?php echo "arr".$i.$j;?>>
			<br/><br/>

	<?php
	
		}
	}

	?>

			<input type="submit" name="enviar" value="Enviar">
		</fieldset>
	</form>

	<?php

	}

	else {

		for ($i=0; $i < 3; $i++) { 
			for ($j=0; $j < 5; $j++) {
				$arr[$i][$j] = $_POST["arr$i$j"];
			}
		}

		echo "<h2>Array ya cargado</h2>";

		for ($i=0; $i<3; $i++) {
			for ($j=0; $j<5; $j++) {
				echo ($arr[$i][$j]." ");
			}
			echo "<br/>";
		}

		echo "<h2>Array colocado por fila</h2>";

		for ($i=0; $i<3; $i++) {
			for ($j=0; $j<5; $j++) {
				echo ($arr[$i][$j]." ");
			}
		}

		echo "<h2>Array colocado por columna</h2>";

		for ($i=0; $i<5; $i++) {
			for ($j=0; $j<3; $j++) {
				echo ($arr[$j][$i]." ");
			}
		}

	}

	?>	
</body>
</html>