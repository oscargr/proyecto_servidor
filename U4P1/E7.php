<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ejercicio 7</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
</head>
<body>

	<?php

	if (!isset($_POST['gen'])) {

	?>

	<form action="" method="POST">

	<?php

		for ($i=0; $i<3; $i++) {
			$color = substr(md5(rand()), 0, 6);

	?>
		
			<p>Título de la web: <input type="text" name="titulo[]"> URL a mostrar: <input type="url" name="url[]"> Color: <input type="color" name="color[]" value=<?php echo "#".$color; ?>></p>

	<?php

		}

	?>
			<p>Orientación:</p>
			<p><input type="radio" name="ori" value="horizontal">Horizontal</p>
			<p><input type="radio" name="ori" value="vertical">Vertical</p>
			<p><input type="submit" name="gen" value="Generar"></p>
		</form>

	<?php

	}
	else {

		class Opcion {

			private $titulo;
			private $enlace;
			private $bgColor;

			public function constructor($nTitulo,$nEnlace,$nBgColor) {
				$this->titulo=$nTitulo;
				$this->enlace=$nEnlace;
				$this->bgColor=$nBgColor;
			}

			public function graficar() {
				return "<a href='".$this->enlace."' style='background-color:".$this->bgColor."'>".$this->titulo."</a>";
			}

		}

		class Menu extends Opcion{

			private $opciones = array();
			private $orientacion;

			public function addOp($nOp) {
				array_push($this->opciones, $nOp);
			}

			public function mostrarOp($orientacion) {
				if ($orientacion=="horizontal") {
					foreach ($this->opciones as $op) {
						echo $op->graficar();
					}
				}
				else {
					foreach ($this->opciones as $op) {
						echo $op->graficar()."<br>";
					}
				}
			}

		}

		foreach ($_POST['titulo'] as $ind => $valor) {
			${'opcion'.$ind}=new Opcion;
			${'opcion'.$ind}->constructor($valor,$_POST['url'][$ind],$_POST['color'][$ind]);
		}

		$menu=new Menu;

		$menu->addOp($opcion0);

		$menu->addOp($opcion1);

		$menu->addOp($opcion2);

		echo $menu->mostrarOp($_POST['ori']);

	}

	?>

</body>
</html>