<?php

class empleado {

	private $nombre;
	private $sueldo;

	public function nombrarEmpleado($nuevoNombre) {
		$this->nombre=$nuevoNombre;
	}

	public function mostrarNombre() {
		return $this->nombre."<br>";
	}

	public function definirSueldo($nuevoSueldo) {
		$this->sueldo=$nuevoSueldo;
	}

	public function mostrarSueldo() {
		return $this->sueldo."<br>";
	}

	public function impuestos() {
		if ($this->sueldo>3000) {
			return $this->nombre."-Debe pagar impuestos<br>";
		}
		else {
			return $this->nombre."-No paga impuestos<br>";
		}
	}

}

$emple1 = new empleado;

$emple1->nombrarEmpleado("Luis");

$emple1->definirSueldo(2500);

$emple2 = new empleado;

$emple2->nombrarEmpleado("Carla");

$emple2->definirSueldo(4300);

echo $emple1->impuestos();
var_dump($emple1);
echo "<br>";
echo $emple2->impuestos();
var_dump($emple2);

?>