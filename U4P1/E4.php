<?php

class Persona {

	private $nombre;
	private $edad;

	public function cargarDatos($nuevoNombre, $nuevaEdad) {
		$this->nombre=$nuevoNombre;
		$this->edad=$nuevaEdad;
	}

	public function mostrarDatos() {
		return "<p>Datos personales de la persona: </p><p>Nombre: ".$this->nombre."</p><p>Edad: ".$this->edad."</p>";
	}

}

class Empleado extends Persona {

	private $sueldo;

	public function setSueldo($nuevoSueldo) {
		$this->sueldo=$nuevoSueldo;
	}

	public function getSueldo() {
		return "<p>Sueldo: ".$this->sueldo."€</p>";
	}

}

$p1 = new Persona;

$p1->cargarDatos("Rodriguez, Pablo",24);

echo $p1->mostrarDatos();

$e1 = new Empleado;

$e1->cargarDatos("Gonzalez, Ana",32);

$e1->setSueldo(2400);

echo $e1->mostrarDatos();

echo $e1->getSueldo();

?>