<?php

class Racional {

	private $numRacional;

	public function introducirRacional($nRacional) {
		$this->numRacional=$nRacional;
	}

	public function mostrarRacional($var) {
		return "<p>Valor de ".$var.": ".$this->numRacional."</p>";
	}

}

$a=new Racional;

$a->introducirRacional("8/5");

echo $a->mostrarRacional('$a');

var_dump($a);

echo "<br>";

$b=new Racional;

$b->introducirRacional("6/4");

echo $b->mostrarRacional('$b');

var_dump($b);

echo "<br>";

?>