<?php

class Opcion {

	private $titulo;
	private $enlace;
	private $bgColor;

	public function constructor($nTitulo,$nEnlace,$nBgColor) {
		$this->titulo=$nTitulo;
		$this->enlace=$nEnlace;
		$this->bgColor=$nBgColor;
	}

	public function graficar() {
		return "<a href='".$this->enlace."' style='background-color:".$this->bgColor."'>".$this->titulo."</a>";
	}

}

class Menu extends Opcion{

	private $opciones = array();
	private $orientacion;

	public function addOp($nOp) {
		array_push($this->opciones, $nOp);
	}

	public function mostrarOp($orientacion) {
		if ($orientacion==0) {
			foreach ($this->opciones as $op) {
				echo $op->graficar();
			}
		}
		else {
			foreach ($this->opciones as $op) {
				echo $op->graficar()."<br>";
			}
		}
	}

}

$google=new Opcion;

$google->constructor("Google","https://www.google.es","#C3D9FF");

$lne=new Opcion;

$lne->constructor("La Nueva España","https://www.lne.es","#CDEB8B");

$lvda=new Opcion;

$lvda->constructor("La Voz de Avilés","https://www.elcomercio.es/aviles/","#C3D9FF");

$menu=new Menu;

$menu->addOp($google);

$menu->addOp($lne);

$menu->addOp($lvda);

echo $menu->mostrarOp(0);

?>