<?php

class Libros {

	private $titulo;
	private $autor;
	private $apublicacion;
	private $hojas;
	private $editorial;

	public function introducirLibro($nTitulo, $nAutor, $nApublicacion, $nHojas, $nEditorial) {
		$this->titulo=$nTitulo;
		$this->autor=$nAutor;
		$this->apublicacion=$nApublicacion;
		$this->hojas=$nHojas;
		$this->editorial=$nEditorial;
	}

	public function verLibro() {
		return "<h2>Título: ".$this->titulo."</h2><p>Autor: ".$this->autor."</p><p>Año de publicación: ".$this->apublicacion."</p><p>Hojas: ".$this->hojas."</p><p>Editorial: ".$this->editorial."</p>";
	}

}

$libro1 = new Libros;

$libro1->introducirLibro("Los pilares de la tierra","Ken Follet","2001",1040,"Plaza & Janes Editores");

echo $libro1->verLibro();

?>